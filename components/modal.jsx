import React from "react";
import { Button, Modal, Center, NativeBaseProvider } from "native-base";
import { useState } from "react";
import { Circle } from "react-native-svg";
import { Text } from "react-native";
const ModalWithButton = ({ body }) => {
  const [showModal, setShowModal] = useState(false);
  return (
    <>
      <Button onPress={() => setShowModal(true)}>Button</Button>
      <Modal isOpen={showModal} onClose={() => setShowModal(false)}>
        <Modal.Content maxWidth="400px">
          <Modal.CloseButton />
          <Modal.Header>Modal Title</Modal.Header>
          <Modal.Body>{body}</Modal.Body>
          <Modal.Footer>
            <Button.Group variant="ghost" space={2}>
              <Button>LEARN MORE</Button>
              <Button
                onPress={() => {
                  setShowModal(false);
                }}
              >
                ACCEPT
              </Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
    </>
  );
};

const BaseModal = ({ showModal, setShowModal, body }) => {
  return (
    <>
      <Modal isOpen={showModal} onClose={() => setShowModal(false)}>
        <Modal.Content maxWidth="400px">
          <Modal.CloseButton />
          <Modal.Header>Modal Title</Modal.Header>
          <Modal.Body>dfjblksafds</Modal.Body>
          <Modal.Footer>
            <Button.Group variant="ghost" space={2}>
              <Button>LEARN MORE</Button>
              <Button
                onPress={() => {
                  setShowModal(false);
                }}
              >
                ACCEPT
              </Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
    </>
  );
};

export const SummaryPointModal = ({ point }) => {
  const [showModal, setShowModal] = useState(false);
  const pointJson = point.json;
  return (
    <>
      <BaseModal
        body={JSON.stringify(point)}
        showModal={showModal}
        setShowModal={setShowModal}
      />

      <Circle
        key={point.id}
        cx={pointJson.pointX}
        cy={pointJson.pointY}
        r="5"
        stroke={pointJson.pointColor}
        strokeWidth="6"
        fill="transparent"
        // onPress={() => }
      />
    </>
  );
};
