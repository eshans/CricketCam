import React, { Component } from "react";
import {
  capitalizeFirstLetter,
  getPointXFromX,
  getPointYFromY,
  getXDistanceFromPointX,
  getXFromPointX,
  getXPointDistance,
  getYDistanceFromPointY,
  getYFromPointY,
  getYPointDistance,
  to2DecimalPlaces,
} from "../constants/utils";
import {
  INITIAL_POINT_STATE,
  IPHONE_MODE,
  SELECT_MARKER_COLORS,
  INITIAL_GRID_PARAMS as initialState,
  LENGTH_UNIT,
  STYLES,
  DISABLED_COLOR,
  PRIMARY_COLOR,
  TOAST_PROPS,
  BATTER_POSITIONS,
  DEVELOPER_MODE,
} from "../constants/values";
import DataGrid from "./dataGrid";
import {
  View,
  Text,
  Dimensions,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { Button, Icon, Picker, Toast } from "native-base";
import { addPoint } from "./database/points";
import { getParticipants } from "./database/participants";
import { SlideDownView, SlideFromRightView } from "../screens/miscComponents/Fade";

export class AddPoints extends Component {
  get_button_mapping = () => {
    const obj = {};
    [...SELECT_MARKER_COLORS[0], ...SELECT_MARKER_COLORS[1]].map((color) => {
      obj[color] = capitalizeFirstLetter(color);
    });
    return obj;
  };

  state = {
    ...initialState,
    ...INITIAL_POINT_STATE,
    ...this.props,
    selectButtonEdit: false,
    button_mapping: this.get_button_mapping(),
  };

  componentDidMount() {
    // console.log("mount ", { state: this.state, props: this.props });
    getParticipants((arr) => {
      this.setState({ ...this.state, participants: arr });
    });
    this.setState({
      ...this.state,
      button_mapping: this.get_button_mapping(),
    });
    // console.log(this.state.button_mapping);
  }

  componentDidUpdate() {
    if (!this.state.button_mapping) {
      this.setState({
        ...this.state,
        button_mapping: this.get_button_mapping(),
      });
    }
    // console.log("updated add points");
    // console.log("mount ", { state: this.state, props: this.props });
  }

  shouldComponentUpdate() {
    return true;
  }

  toggleSelectButtonEdit = () => {
    if (!this.state.selectButtonEdit) {
      this.setState({ selectButtonEdit: true });
    } else {
      this.setState({ selectButtonEdit: false });
    }
  };

  addPointToDatabase = (color) => {
    // console.log("adding point ", {
    //   pointX: this.props.pointX,
    //   pointY: this.props.pointY,
    //   pointColor: color,
    //   "Ball no": this.state["Ball no"],
    //   Type: this.state.Type,
    //   Runup: this.state.Runup,
    //   Batter: this.state.Batter,
    //   Comment: this.state.Comment,
    // });

    const pointX = getXFromPointX(this.props.pointX, this.state.scale);
    const pointY = getYFromPointY(this.props.pointY, this.state.scale);

    addPoint(this.props.play, {
      pointX: pointX,
      pointY: pointY,
      pointColor: color,
      "Ball no": this.state["Ball no"],
      Type: this.state.Type,
      Runup: this.state.Runup,
      Batter: this.state.Batter,
      Comment: this.state["Comment"],
      Length: this.state["Length"],
      Line: this.state["Line"],
      Quality: this.state["Quality"],
      Speed: this.state["Speed"],
    });
    Toast.show({
      text: `Point saved!`,
      ...TOAST_PROPS("success"),
    });
  };

  render() {
    const SideTableButton = ({ field, value }) => {
      return (
        <Button
          // key={`${field}=${value}`}
          style={{
            margin: 2,
            padding: 5,
            backgroundColor:
              this.state[field] === value ? PRIMARY_COLOR : DISABLED_COLOR,
            height: 30,
          }}
          onPress={() => {
            const newState = this.state;
            newState[field] = value;
            this.setState(newState);
            this.setState({ ...this.state, update: Math.random() });
            // console.log('values set ',value)
          }}
        >
          <Text
          // key="dsjnfdsaf"
          >
            {value}
          </Text>
        </Button>
      );
    };

    const data = [
      {
        title: "Participant",
        field: (
          <Picker
            key={"I-participant"}
            style={[STYLES.input, { width: 200, height: 35 }]}
            enabled={false}
            selectedValue={this.state.participant}
            mode="dialog"
            onValueChange={(val, index) => {
              this.state.participant = val;
            }}
          >
            {this.state.participants &&
              this.state.participants.map((val) => (
                <Picker.Item key={val.id} label={val.name} value={val.name} />
              ))}
          </Picker>
        ),
      },
      {
        title: "Ball no.",
        field: (
          <TextInput
            key={"I-Ball no"}
            style={styles.numberInput}
            onChangeText={(e) => {
              // this.state.Comment = e;
              this.setState({ ...this.state, "Ball no": e });
            }}
            value={this.state["Ball no"]}
          />
        ),
      },

      {
        title: "Batter",
        field: (
          <View style={{ flex: 1, flexDirection: "row" }}>
            {BATTER_POSITIONS.map((val) => {
              return <SideTableButton key={val} field={"Batter"} value={val} />;
            })}
          </View>
        ),
      },

      {
        title: "Run-up",
        field: (
          <View style={{ flex: 1, flexDirection: "row" }}>
            {["Over", "Around"].map((val) => {
              return <SideTableButton key={val} field={"Runup"} value={val} />;
            })}
          </View>
        ),
      },
      {
        title: "Length",
        field: (
          <View style={{ flex: 1, flexDirection: "row" }}>
            {["Yorker", "Full", "Good", "Short"].map((val) => {
              return <SideTableButton key={val} field={"Length"} value={val} />;
            })}
          </View>
        ),
      },
      {
        title: "Line",
        field: (
          <TextInput
            key={"I-Line"}
            style={styles.numberInput}
            onChangeText={(e) => {
              // this.state.Comment = e;
              this.setState({ ...this.state, Line: e });
            }}
            value={this.state["Line"]}
          />
        ),
      },
      {
        title: "Quality",
        field: (
          <View style={{ flex: 1, flexDirection: "row" }}>
            {["Length", "Line", "L&L", "Error"].map((val) => {
              return (
                <SideTableButton key={val} field={"Quality"} value={val} />
              );
            })}
          </View>
        ),
      },
      {
        title: "Speed",
        field: (
          <TextInput
            key={"I-Line"}
            style={styles.numberInput}
            onChangeText={(e) => {
              // this.state.Comment = e;
              this.setState({ ...this.state, Speed: e });
            }}
            value={this.state["Speed"]}
          />
        ),
      },
      {
        title: "Type",
        field: (
          <TextInput
            key={"I-Type"}
            style={styles.numberInput}
            onChangeText={(e) => {
              // this.state.Comment = e;
              this.setState({ ...this.state, Type: e });
            }}
            value={this.state["Type"]}
          />
        ),
      },
      {
        title: "Comment",
        field: (
          <TextInput
            key={"I-Comment"}
            style={styles.numberInput}
            onChangeText={(e) => {
              // this.state.Comment = e;
              this.setState({ ...this.state, Comment: e });
            }}
            value={this.state["Comment"]}
          />
        ),
      },
    ];
    if (this.props.show) {
      return (
        <SlideDownView style={{ flex: 1, borderRadius: 100, backgroundColor:'transparent'}} key={"addpoints-view1"}>
          <View
            style={{ flex: 1, flexDirection: "row", backgroundColor: 'transparent', borderRadius: 100}}
            key={"addpoints-view3"}
          >
            {/* <Swiper> */}
            <View
              key={"addpoints=view4"}
              style={{
                alignItems: "center",
                justifyContent: "center",
                width: Dimensions.get("window").width / 2,
                backgroundColor: 'transparent'
              }}
            >
              <View style={styles.topActions} key={"addpoints-view5"}>
                {/* <View style={styles.controlContainer}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.setState({ ...this.state, actualLength: null });
                  }}
                >
                  <Text>{`Actual length (${LENGTH_UNIT})`}</Text>
                </TouchableWithoutFeedback>

                <View style={styles.buttonContainer}>
                  <TextInput
                    style={styles.numberInput}
                    onChangeText={(e) => {
                      this.state.actualLength = e;
                    }}
                    // onBlur={(e) => this.setState({ ...this.state, actualWidth: e })}
                    value={this.state.actualLength}
                    placeholder="Length"
                    keyboardType="numeric"
                  />
                </View>
              </View>
              <View style={styles.controlContainer}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.setState({ ...this.state, actualWidth: null });
                  }}
                >
                  <Text>{`Actual width (${LENGTH_UNIT})`}</Text>
                </TouchableWithoutFeedback>
                <View style={styles.buttonContainer}>
                  <TextInput
                    style={styles.numberInput}
                    onChangeText={(e) => {
                      this.state.actualWidth = e;
                    }}
                    value={this.state.actualWidth}
                    placeholder="Width"
                    keyboardType="numeric"
                  />
                </View>
              </View> */}

                {!IPHONE_MODE ? (
                  <View style={styles.controlContainer} key={"fpbdsafds"}>
                    <Text
                      style={{
                        fontSize: 60,
                        textAlign: "center",
                        paddingBottom: 50,
                      }}
                    >
                      Delivery Assesment
                    </Text>
                    <View style={styles.buttonContainer} key={"kfjbdlafd"}>
                      <Text>
                        {`Y distance: ${to2DecimalPlaces(getYPointDistance(
                          this.props.pointY,
                          this.props.borderPoints,
                          this.props.actualLength
                        ))}${LENGTH_UNIT} | `}
                      </Text>
                      <Text>
                        {`X distance: ${to2DecimalPlaces(getXPointDistance(
                          this.props.pointX,
                          this.props.borderPoints,
                          this.props.actualWidth,
                          this.props.pointY,
                        ))}${LENGTH_UNIT} `}
                      </Text>
                    </View>
                    {DEVELOPER_MODE && (
                      <View style={styles.buttonContainer} key={"fdjbpbkfdsa"}>
                        <Text>
                          {`Y pixel: ${to2DecimalPlaces(
                            this.props.pointY
                          )}${LENGTH_UNIT} | `}
                        </Text>
                        <Text>
                          {`X pixel: ${to2DecimalPlaces(
                            this.props.pointX
                          )}${LENGTH_UNIT} `}
                        </Text>
                      </View>
                    )}
                    {/* <View style={styles.buttonContainer} key={"njfdbklaf"}>
                        <Text>
                          {`Y inversed: ${getPointYFromY(
                            getYFromPointY(this.props.pointY, this.props.scale),
                            this.props.scale
                          )}${LENGTH_UNIT} | `}
                        </Text>
                        <Text>
                          {`X inversed: ${getPointXFromX(
                            getXFromPointX(this.props.pointX, this.props.scale),
                            this.props.scale
                          )}${LENGTH_UNIT} `}
                        </Text>
                      </View> */}
                  </View>
                ) : null}
              </View>
              {SELECT_MARKER_COLORS.map((colorSet, index) => {
                return (
                  <View style={styles.topActions} key={index}>
                    {colorSet.map((color) => (
                      <View style={styles.controlContainer} key={color}>
                        {/* <TouchableWithoutFeedback
                    onPress={() => {
                      this.setState({ ...this.state, actualWidth: null });
                    }}
                  >
                    <Text>Point color</Text>
                  </TouchableWithoutFeedback> */}
                        <View
                          style={styles.buttonContainer}
                          key={`${color}-bfhdilafd`}
                        >
                          {/* <Picker
                      style={{
                        margin: 5,
                        borderColor: "#aaaaaa",
                        borderWidth: 1,
                        borderRadius: 5,
                      }}
                      enabled={false}
                      selectedValue={color}
                    >
                      <Picker.Item
                        key={color}
                        label={capitalizeFirstLetter(color)}
                        value={color}
                      />
                    </Picker> */}
                        </View>
                        <View
                          style={styles.controlContainer}
                          key={`${color}-bfhdfdsfilafd`}
                        >
                          {this.state.selectButtonEdit ? (
                            <TextInput
                              key={"color"}
                              style={[
                                styles.numberInput,
                                { backgroundColor: color },
                              ]}
                              onChangeText={(e) => {
                                const obj = this.state.button_mapping;
                                obj[color] = e;
                                this.state = {
                                  ...this.state,
                                  button_mapping: obj,
                                };
                              }}
                              //  value={this.state["Type"]}
                            />
                          ) : (
                            <TouchableOpacity
                              key={`${color}-fdsafdsa`}
                              onPress={() => this.addPointToDatabase(color)}
                              // onLongPress={() => }

                              style={{
                                padding: 10,
                                width: 80,
                                alignText: "center",
                                backgroundColor: color,
                                borderRadius: 5,
                              }}
                            >
                              <Text>{this.state.button_mapping[color]}</Text>
                            </TouchableOpacity>
                          )}
                        </View>
                      </View>
                    ))}
                  </View>
                );
              })}
              <Icon
                title="button"
                name="create-outline"
                onPress={this.toggleSelectButtonEdit}
              ></Icon>
            </View>
            <View
              key={"datagrid-view2"}
              style={{
                alignItems: "center",
                justifyContent: "center",
                width: (Dimensions.get("window").width * 5) / 8,
                paddingTop: 70,
                paddingBottom: 20,
              }}
            >
              <View style={styles.topActions} key={"datagrid-view"}>
                <DataGrid
                  showBorder={true}
                  data={data}
                  showHeader={false}
                  key={"datagrid"}
                />
              </View>
            </View>
            {/* </Swiper> */}
          </View>
        </SlideDownView>
      );
    } else
      return (
        <SlideDownView style={{ backgroundColor: "transparent", zIndex:0, height: 0 }}></SlideDownView>
      );
  }
}

const styles = {
  scrollView: {
    maxHeight: 400,
  },
  container: {
    position: "absolute",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    zIndex: 100,
    top: initialState.translateZ,
    // backgroundColor: "blue",
  },

  rotateView: {
    justifyContent: "center",
    alignItems: "center",
    zIndex: 1000,
    // position: "absolute",
    backgroundColor: "rgba(0, 52, 0, 0.5)",
    zIndex: 100,
    transform: [
      { perspective: initialState.perspective * 100 },
      { rotateX: `${initialState.rotateX}deg` },
    ],
    width: initialState.width * (parseFloat(initialState.scale) / 10),
    height: initialState.height * (parseFloat(initialState.scale) / 10),
  },
  // rotateView2: {
  //   justifyContent: "center",
  //   alignItems: "center",
  //   zIndex: 1000,
  //   position: "absolute",
  //   backgroundColor: "rgba(0, 52, 0, 0.5)",
  //   zIndex: 100,
  //   transform: [
  //     { perspective: 1000 },
  //     { rotateX: `${initialState.rotateX + 20}deg` },
  //   ],
  //   width: initialState.width * (parseFloat(initialState.scale) / 10),
  //   height: initialState.height * (parseFloat(initialState.scale) / 10),
  // },
  topActions: {
    marginRight: 100,
    marginLeft: 100,
    flexDirection: "row",
    // justifyContent: "space-between",
    backgroundColor: "transparent",
    zIndex: 1000,
  },
  controlContainer: {
    padding: 10,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'transparent'
  },
  buttonContainer: {
    flex: 0,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  numberInput: {
    margin: 2,
    borderColor: "#aaaaaa",
    borderWidth: 1,
    borderRadius: 5,
    padding: 7,
    width: 100,
  },

  splitRight: {
    right: 0,
    width: Dimensions.get("window").width / 2,
    position: "fixed",
    zIndex: 1,
    top: 0,
    // overflowX: "hidden",
    paddingTop: 20,
  },
  splitLeft: {
    left: 0,
    width: Dimensions.get("window").width / 2,
    position: "fixed",
    zIndex: 1,
    top: 0,
    // overflowX: "hidden",
    paddingTop: 20,
  },
  centered: {
    position: "absolute",
    alignItems: "center",
    // transform: translate("-50%", "-50%"),
    textAlign: "center",
  },
};
