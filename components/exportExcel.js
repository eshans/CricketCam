import React from "react";
import XLSX from "xlsx";
import * as FileSystem from "expo-file-system";
import * as Sharing from "expo-sharing";
import { Alert } from "react-native";
import { Toast } from "native-base";
import { TOAST_PROPS } from "../constants/values";
import { tryCatchFunction, tryCatchFunctionAsync } from "../constants/utils";

// var data = [
//   {
//     name: "John",
//     city: "Seattle",
//   },
//   {
//     name: "Mike",
//     city: "Los Angeles",
//   },
//   {
//     name: "Zach",
//     city: "New York",
//   },
// ];

export const excelExport = async (data, name) => {
  tryCatchFunctionAsync(async () => {
    var ws = XLSX.utils.json_to_sheet(data);
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Analysis");
    const wbout = XLSX.write(wb, {
      type: "base64",
      bookType: "xlsx",
    });
    const uri = FileSystem.cacheDirectory + name + ".xlsx";
    console.log(`Writing to ${JSON.stringify(uri)} with text: ${wbout}`);
    await FileSystem.writeAsStringAsync(uri, wbout, {
      encoding: FileSystem.EncodingType.Base64,
    });

    await Sharing.shareAsync(uri, {
      mimeType:
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      dialogTitle: "Video analysis data",
      UTI: "com.microsoft.excel.xlsx",
    });
  }, `Export successful!`);
};
