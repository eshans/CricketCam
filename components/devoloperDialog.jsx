import { Toast, Icon, Right } from "native-base";
import React, { useState } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  Pressable,
  View,
  TextInput,
  Dimensions,
  ScrollView,
} from "react-native";
import { STYLES, TOAST_PROPS, VIDEO_LOCATION } from "../constants/values";
import { addParticipant, getParticipants } from "./database/participants";
import * as FileSystem from "expo-file-system";
import { clearAllItemsFromDatabase } from "./database/items";
import { clearAllPointsFromDatabase, getAllPoints, getPoints } from "./database/points";

const PAGE_HELP_CONTENT = {
  videoRecorder: `This is videoRecorder help text`,
  RecordGridControls: `this is RecordGridControls help text`,
  videoPlayer: `this is videoPlayer help text`,
  videosList: "this is videosList help text",
  summaryView: `this is summaryView help text`,
};

const DeveloperSettingsButton = ({ screen, modalVisible, setModalVisible }) => {
  

  return (
    <View
      style={{
        alignItems: "right",
        position: "absolute",
        width: 80,
        height: 100,
        zIndex: 20000,
        top: 0,
        right: 0,
      }}
    >
      
      <View style={styles.centeredView}>
        <DeveloperDialog
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          screen={screen}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 5,
    padding: 10,
    // alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  scrollView: {
    flexGrow: 0,
  },
  button: {
    borderRadius: 5,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "black",
  },
  buttonClose: {
    backgroundColor: "black",
    margin: 5,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    margin: 10,
    textAlign: "center",
  },
});

export default DeveloperSettingsButton;

export const DeveloperDialog = ({ modalVisible, setModalVisible, screen }) => {
  // console.log({ modalVisible });

  const [text, setText] = useState();

  const loadVideosList = async () => {
    const dirInfo = await FileSystem.getInfoAsync(
      FileSystem.documentDirectory + "videos"
    );
    if (!dirInfo.exists) {
      console.log("videos directory doesnt exist");
      this.setState({ videos: [] });
      Toast.show({
        text: "Videos directory doesnt exist. Creating now...",
        ...TOAST_PROPS("info"),
      });
      await FileSystem.makeDirectoryAsync(
        `${FileSystem.documentDirectory}videos/`,
        {
          intermediates: true,
        }
      ).then(() => {
        this.loadVideosList();
      });
    }

    const videos = await FileSystem.readDirectoryAsync(
      FileSystem.documentDirectory + "videos"
    );

    setText(videos);
  };

  async function clearAllItemsFromDirectory() {

    console.log('Deleting all video files...');
    await FileSystem.deleteAsync(VIDEO_LOCATION);
    await FileSystem.makeDirectoryAsync(
      VIDEO_LOCATION,
      {
        intermediates: true,
      }
    )
    Toast.show({
      text: "Deleted all videos from directory",
      ...TOAST_PROPS("info"),
    });

  }

  const loadPointsInDatabase = () => {
    getAllPoints(setText);
  };

  const loadParticipantsInDatabase = () => {
    getParticipants(setText);
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
        setModalVisible(!modalVisible);
      }}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>Developer options</Text>
          <ScrollView style={styles.scrollView}>
            <Text style={styles.modalText}>
              These actions can break the app. Use only if you know what you're
              doing.
            </Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => clearAllItemsFromDatabase()}
            >
              <Text style={styles.textStyle}>
                Delete all video records from database
              </Text>
            </Pressable>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => clearAllItemsFromDirectory()}
            >
              <Text style={styles.textStyle}>
                Delete all video records from directory
              </Text>
            </Pressable>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => clearAllPointsFromDatabase()}
            >
              <Text style={styles.textStyle}>
                Delete all point records from database
              </Text>
            </Pressable>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => loadVideosList()}
            >
              <Text style={styles.textStyle}>
                Videos list in videos directory
              </Text>
            </Pressable>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => loadVideosListDatabase()}
            >
              <Text style={styles.textStyle}>Videos list in database</Text>
            </Pressable>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => loadPointsInDatabase()}
            >
              <Text style={styles.textStyle}>Points list in database</Text>
            </Pressable>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => loadParticipantsInDatabase()}
            >
              <Text style={styles.textStyle}>Participant list in database</Text>
            </Pressable>
          </ScrollView>
          <ScrollView style={styles.scrollView}>
            <Text>
              {text &&
                JSON.stringify(text)
                  // .replaceAll("],[", "],\n[")
                  // .replaceAll(",", ",\n")
                  }
            </Text>
          </ScrollView>
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => setModalVisible(!modalVisible)}
          >
            <Text style={styles.textStyle}>Close</Text>
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};
