import React, { Component } from "react";
import VideoPlayer from "expo-video-player";
import { Video } from "expo-av";

import * as FileSystem from "expo-file-system";
import { HEIGHT, STYLES, VIDEO_LOCATION } from "../constants/values";

export default class Player extends Component {
  shouldComponentUpdate(prevProps, prevState) {
    if (prevProps._video != this.props._video) {
      return true
    }
    return false;
  }

  

  videoProps = {
    onPlaybackStatusUpdate: (status) => this.props.onPlaybackStatusUpdate(status),
    ref: this.props._video,
    resizeMode: Video.RESIZE_MODE_COVER,
    shouldPlay: true,
    source: {
      uri:
        this.props.play === "test.mov"
          ? `http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4`
          : `${VIDEO_LOCATION}/${this.props.play}`,
    },
  };
  render() {
    console.log({video2: this.props.play, location:  `${VIDEO_LOCATION}/${this.props.play}`})
    return (
      <Video
        // style={styles.containerCamera}
        {...this.videoProps}
        style={{...STYLES.videoPlayer, ...{backgroundColor: 'blue', }}}
        isPortrait={true}
        key={Math.random()}
      />
    );
  }
}
