import React from "react";
import {
  BORDER_STATE,
  INITIAL_GRID_PARAMS,
  VIDEO_LOCATION,
} from "../constants/values";
import { addItem } from "./database/items";
import * as FileSystem from "expo-file-system";
import { useState } from "react";
import { View, Text } from "./Themed";
import { addParticipant } from "./database/participants";

const AddSampleVideo = () => {
  const [downloadProgress, setDownloadProgress] = useState(0);
  const callback = (downloadProgress) => {
    const progress =
      downloadProgress.totalBytesWritten /
      downloadProgress.totalBytesExpectedToWrite;

    if ((progress * 100) % 10 === 0) {
      console.log("downloadProgress: ", progress);
    }
    setDownloadProgress({
      downloadProgress: progress,
    });
  };

  const vidName = `tutorial-${Math.random()}.mp4`;
  const participant = `participant-${Math.floor((Math.random() * 10) / 3) + 1}`;

  const downloadResumable = FileSystem.createDownloadResumable(
    "http://techslides.com/demos/sample-videos/small.mp4",
    VIDEO_LOCATION + `/${vidName}`,
    {},
    callback
  );

  try {
    const { uri } = downloadResumable.downloadAsync();
    console.log("Finished downloading to ", uri);
  } catch (e) {
    console.error(e);
  }

  try {
    downloadResumable.pauseAsync();
    console.log("Paused download operation, saving for future retrieval");
    // AsyncStorage.setItem(
    //   "pausedDownload",
    //   JSON.stringify(downloadResumable.savable())
    // );
  } catch (e) {
    console.error(e);
  }

  try {
    const { uri } = downloadResumable.resumeAsync();
    console.log("Finished downloading to ", uri);
  } catch (e) {
    console.error(e);
  }

  //To resume a download across app restarts, assuming the the DownloadResumable.savable() object was stored:
  //   const downloadSnapshotJson = await AsyncStorage.getItem("pausedDownload");
  //   const downloadSnapshot = JSON.parse(downloadSnapshotJson);
  //   const downloadResumable = new FileSystem.DownloadResumable(
  //     downloadSnapshot.url,
  //     downloadSnapshot.fileUri,
  //     downloadSnapshot.options,
  //     callback,
  //     downloadSnapshot.resumeData
  //   );

  //   try {
  //     const { uri } = await downloadResumable.resumeAsync();
  //     console.log("Finished downloading to ", uri);
  //   } catch (e) {
  //     console.error(e);
  //   }

  const json = { ...BORDER_STATE, participant: participant };
  addParticipant(participant, (props) => {
    return true;
  });
  addItem(vidName, json);

  return (
    <View>
      <Text>{downloadProgress}</Text>
    </View>
  );
};

export const RunTutorialAdditions = (shouldRun) => {
  console.log("running tutorial additions");
  if (shouldRun) {
    return <AddSampleVideo />;
  } else {
    return <View></View>;
  }
};
