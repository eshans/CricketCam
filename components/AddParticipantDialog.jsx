import { Toast, Icon, Right } from "native-base";
import React, { useState } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  Pressable,
  View,
  TextInput,
} from "react-native";
import { log } from "../constants/utils";
import { STYLES, TOAST_PROPS } from "../constants/values";
import { addParticipant } from "./database/participants";

const AddParticipantButton = ({
  isIcon,
  setParticipants,
  setParticipantInRotateView,
}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [participant, setParticipant] = useState("");
  const [details, setDetails] = useState("")
  const onAddParticipant = () => {
    try {log('onAddParticipant, addParticipant.jsx')

      addParticipant(participant,() => {} ,{details});
      setParticipants(participant);
      setParticipantInRotateView(participant);
      setParticipant("");
    } catch (err) {
      console.log(err);
      Toast.show({
        text: "Error: couldnt add participant",
        ...TOAST_PROPS('error'),
      });
    }
  };

  return (
    <View style={{ padding: 10 }}>
      {isIcon ? (
        <Icon name="add-sharp" onPress={() => setModalVisible(true)}></Icon>
      ) : (
        <Pressable
          style={[styles.button, styles.buttonOpen]}
          onPress={() => setModalVisible(true)}
        >
          <Text style={styles.textStyle}>Add Participant</Text>
        </Pressable>
      )}
      <View style={styles.centeredView}>
        <AddParticipantDialog
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          onAddParticipant={onAddParticipant}
          participant={participant}
          setParticipant={setParticipant}
          details={details}
          setDetails={setDetails}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 5,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 5,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "black",
  },
  buttonClose: {
    backgroundColor: "black",
    margin: 5,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});

export default AddParticipantButton;

export const AddParticipantDialog = ({
  modalVisible,
  setModalVisible,
  onAddParticipant,
  participant,
  setParticipant,
  details,
  setDetails
}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
        setModalVisible(!modalVisible);
      }}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>Add participant</Text>
          <TextInput
            style={STYLES.input}
            onChangeText={(e) => setParticipant(e)}
            // onBlur={(e) => setState({ ...state, actualWidth: e })}
            value={participant}
            placeholder="Participant"
          />
          <TextInput
            style={STYLES.input}
            onChangeText={(e) => setDetails(e)}
            // onBlur={(e) => setState({ ...state, actualWidth: e })}
            value={details}
            placeholder="Details"
          />
          <View style={{ flex: 0, flexDirection: "row" }}>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => {
                onAddParticipant();
                setModalVisible(!modalVisible);
              }}
            >
              <Text style={styles.textStyle}>Add</Text>
            </Pressable>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Close</Text>
            </Pressable>
          </View>
        </View>
      </View>
    </Modal>
  );
};
