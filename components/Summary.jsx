import React, { useEffect, useState } from "react";
import RotateView from "../screens/rotateVIew";
import {
  View,
  Dimensions,
  ActivityIndicator,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { Chip } from "react-native-elements";
import {
  Button,
  Text,
  // Header,
  Body,
  Icon,
  Title,
  Left,
  Right,
  Card,
  CardItem,
  Switch,
} from "native-base";
import {
  BATTER_POSITIONS,
  DEVELOPER_MODE,
  DISABLED_COLOR,
  INITIAL_GRID_PARAMS,
  PRIMARY_COLOR,
  STYLES,
  SUMMARY_ROTATEVIEW_SCALE,
} from "../constants/values";
import { getPoints, deletePoint } from "./database/points";
import HelpButton from "./HelpDialog";
import DataGrid from "./dataGrid";
import {
  getPointXFromX,
  getPointYFromY,
  getXDistanceFromPointX,
  getYaluesOfDistanceRanges,
  getYDistanceFromPointY,
  log,
  to1DecimalPlaces,
  to2DecimalPlaces,
} from "../constants/utils";
import { Touchable } from "react-native";
import {
  changeParticipantDetails,
  getParticipantDetails,
} from "./database/participants";
import _ from "lodash";
import {
  SlideFromLeftView,
  SlideFromRightView,
  SlideUpView,
} from "../screens/miscComponents/Fade";
import { EmptyComponent } from "../screens/miscComponents/ListEmpty";

const styles = {
  bottomActions: STYLES.bottomActions,
  bottomButton: {
    zIndex: 1000,
    width: 100,
    margin: 10,
  },
  containerCenter: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    // width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
};

// const SummaryTable = ({ points }) => {
// log({ points });

//   const formattedPoints = points?.map((point) => {
//     const json = JSON.parse(point.json);
//     return {
//       "Ball no": json["Ball no"],
//       Type: json["Type"],
//       Runup: json["Runup"],
//       Batter: json["Batter"],
//       Comment: json["Comment"],
//     };
//   });
//   return <DataGrid data={points} />;
// };

export const Summary = ({
  plays,
  savedState,
  myVideosSetState,
  summaryType,
}) => {
  const [points, setPoints] = useState();
  const [forceRef, setForceRef] = useState(false);
  const [selected, setSelected] = useState({});
  const [Highlight, setHighlight] = useState(null);
  const [showBallNo, setShowBallNo] = useState(true);

  const forceRefresh = () => {
    setForceRef(!forceRef);
  };

  // log({ plays, myVideosSetState });

  const flatGridState = {
    ...INITIAL_GRID_PARAMS,
    // scale: savedState?.scale,
    rotateX: 78,
    rotateY: 0,
    rotateZ: 0,
    translateX: 0,
    translateY: -100,
    translateZ: -400, //translate Z is done to parent container
    perspective: 20,
    scale: SUMMARY_ROTATEVIEW_SCALE,
    // width: savedState?.width,
    // height: savedState?.height,
    XGrids: savedState?.XGrids,
    YGrids: savedState?.YGrids,
    XSubGrids: savedState?.XSubGrids,
    YSubGrids: savedState?.YSubGrids,
    actualLength: savedState?.actualLength,
    actualWidth: savedState?.actualWidth,
  };

  const formatAndSetpoints = (points) => {
    //Chnage actual values saved in database to pixel values
    const changedPoints = points.map((point) => {
      const json = point.json;
      const pointX = getPointXFromX(json.pointX, SUMMARY_ROTATEVIEW_SCALE);
      const pointY = getPointYFromY(json.pointY, SUMMARY_ROTATEVIEW_SCALE);

      return {
        ...point,
        json: { ...json, pointX, pointY, OldX: json.pointX, OldY: json.pointY },
      };
    });
    log({ changedPoints });
    setPoints(changedPoints);
  };

  useEffect(() => {
    getPoints(
      formatAndSetpoints,
      plays.map((play) => play.value)
    );
  }, [forceRef]);

  const deletePoints = (ids) => {
    ids.map((id) => {
      deletePoint(id);
    });
    forceRefresh();
  };

  const pointsF = points
    ?.sort((a, b) => a.id > b.id)

    .map((point) => {
      // log(point);

      const json = point.json;
      // log(json);
      return {
        id: point.id,
        "Ball no": json["Ball no"],
        Batter: json["Batter"],
        Runup: json["Runup"],
        Length: json.hasOwnProperty("Length") && json["Length"],
        Line: json.hasOwnProperty("Line") && json["Line"],
        Quality: json.hasOwnProperty("Quality") && json["Quality"],
        Speed: json.hasOwnProperty("Speed") && json["Speed"],
        Type: json["Type"],
        Comment: json["Comment"],
        "Y | X":
          String(
            getYDistanceFromPointY(json["pointY"], SUMMARY_ROTATEVIEW_SCALE)
          ) +
          " | " +
          String(
            getXDistanceFromPointX(json["pointX"], SUMMARY_ROTATEVIEW_SCALE)
          ),
      };
    });

  // log("summary", { points });
  // if (!points || !points[0]) {
  //   return <ActivityIndicator />;
  // }
  return (
    <View style={styles.containerCenter}>
      <HelpButton screen={"summaryView"} />
      {pointsF && pointsF[0] ? (
        <>
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              backgroundColor: "white",
            }}
            key={"addpoints-view3"}
          >
            {/* <Swiper> */}

            <View
              key={"rotateview-summary"}
              style={{
                alignItems: "left",
                justifyContent: "center",
                height: (Dimensions.get("window").height * 4) / 8,
              }}
            >
              <SlideFromRightView
                style={{
                  position: "absolute",
                  top: 10,
                  left: 250,
                  width: Dimensions.get("window").width,
                }}
              >
                <RotateView
                  showBallNos={showBallNo}
                  savedState={flatGridState}
                  summaryPoints={points.filter(
                    (item) =>
                      selected.hasOwnProperty(item.id) && selected[item.id]
                  )}
                  onPressPoint={(id) => {
                    setHighlight(id);
                    forceRefresh();
                  }}
                  screen={"summary"}
                />
              </SlideFromRightView>
              <SlideFromLeftView>
                <ParticipantDetailsGrid
                  savedState={savedState}
                  pointsF={pointsF}
                  points={points.filter(
                    (item) =>
                      selected.hasOwnProperty(item.id) && selected[item.id]
                  )}
                />
              </SlideFromLeftView>
            </View>
          </View>
          <View style={{ flexDirection: "row", padding: 20 }}>
            <Text style={{ fontSize: 20 }}>{`Show ball no.`}</Text>
            <Switch
              value={showBallNo}
              onValueChange={() => setShowBallNo(!showBallNo)}
            />
          </View>
          <SlideUpView
            key={"Datagrid-summary"}
            style={{
              alignItems: "center",
              justifyContent: "center",
              height: (Dimensions.get("window").height * 4) / 8,
              paddingTop: Dimensions.get("window").height / 32,
            }}
          >
            <DataGrid
              key={"jbdhskafdska;f "}
              data={pointsF}
              selectable={true}
              selected={selected}
              setSelected={(selected) => {
                setSelected(selected);
                forceRefresh();
              }}
              containerStyles={{
                width: "95%",
                height: Dimensions.get("window").height / 2,
              }}
              showHeader={true}
              showBorder={true}
              exportable={true}
              deletable={true}
              deleteFunction={deletePoints}
              exportFileName={savedState.participant}
              HighLightRowId={Highlight}
            />
          </SlideUpView>
        </>
      ) : (
        // <View style={{height: 400, backgroundColor: 'blue'}}>

        <EmptyComponent
          icon="location"
          mainText="No points to summerize!"
          secondaryText="Add points"
          tertiaryText="by going in to delivery assessment view"
        />
        // </View>
      )}
      {/* <SummaryTable points={points} /> */}

      <SlideUpView style={styles.bottomActions}>
        <Button
          //   disabled={recording}
          style={styles.bottomButton}
          transparent
          onPress={() => myVideosSetState({ viewSummary: null })}
        >
          <Icon name="ios-arrow-back-circle" />
        </Button>
        {/* <Button danger>
          <Icon ios="ios-square" android="md-square" />
        </Button>

        <Button iconLeft transparent style={styles.bottomButton}>
          <Icon ios="ios-recording" android="md-recording" />
          <Text>jdhafdsf</Text>
        </Button> */}
      </SlideUpView>
    </View>
  );
};

const ParticipantDetailsGrid = ({ savedState, pointsF, points }) => {
  const getBatter = (obj) => {
    if (obj) {
      const json = obj.json;
      return json["Batter"];
    }
  };
  const [batterPos, setBatterPos] = useState("");

  const fpoints = points.map((a) => a);

  useEffect(() => {
    const batter = [
      getBatter(fpoints.filter((a) => getBatter(a) == BATTER_POSITIONS[0])[0]),
      getBatter(fpoints.filter((a) => getBatter(a) == BATTER_POSITIONS[1])[0]),
    ].filter((a) => a);
    if (batter.length > 1) {
      setBatterPos("All");
    } else {
      setBatterPos(batter[0]);
    }
    // log({ batter });
  }, [pointsF]);

  const SideTableButton = ({ field, value }) => {
    return (
      <Button
        key={`${field}=${value}`}
        style={{
          margin: 2,
          padding: 5,
          backgroundColor: batterPos === value ? PRIMARY_COLOR : DISABLED_COLOR,
          height: 30,
        }}
        // onPress={() => {
        //   setBatterPos(value);
        // }}
      >
        <Text
        // key="dsjnfdsaf"
        >
          {value}
        </Text>
      </Button>
    );
  };

  return (
    <View
      style={{
        padding: 10,
        margin: 20,
        flexDirection: "column",
        justifyContent: "space-evenly",
        borderWidth: 1,
        borderColor: PRIMARY_COLOR,
        borderRadius: 5,
        height: 500,
        maxWidth: 450,
        width: 450,
        overflow: "scroll",
      }}
    >
      <View style={{ flexDirection: "row", padding: 10 }}>
        <Text style={{ fontSize: 20, fontWeight: "bold" }}>
          {savedState.participant}
        </Text>
      </View>
      <View style={{ flexDirection: "row", padding: 10 }}>
        <Text style={{ fontSize: 20 }}>{`Batter :`}</Text>
        {[...BATTER_POSITIONS, "All"].map((val) => {
          return <SideTableButton key={val} field={"Batter"} value={val} />;
        })}
      </View>
      <ChangeParticipantDetails participant={savedState.participant} />
      {/* <View style={{ flexDirection: "row", padding: 10 }}>
        <Text style={{ fontSize: 20 }}>{`Bowler :`}</Text>
        <Chip title={`${pointsF && pointsF[0]["Ball no"]}`} />
      </View> */}
      <BallSummaryTable points={points} />
    </View>
  );
};

const ChangeParticipantDetails = ({ participant }) => {
  const [initialPDetails, setInitialPDetails] = useState("");
  const [loading, setLoading] = useState(true);

  var details = initialPDetails;

  useEffect(() => {
    getParticipantDetails(participant, setInitialPDetails);
    details = initialPDetails;
    // log({ initialPDetails });
    setLoading(false);
  }, []);

  const onSave = () => {
    changeParticipantDetails(participant, details);
  };

  if (loading) return <ActivityIndicator />;

  return (
    <View style={{ flexDirection: "row", padding: 10 }}>
      <Text style={{ fontSize: 20 }}>{`Details :`}</Text>
      <TextInput
        placeholderTextColor={"black"}
        key={"I-Type"}
        style={STYLES.numberInput}
        onChangeText={(e) => {
          details = e;
        }}
        placeholder={details}
      />
      <Button
        icon
        small
        style={[STYLES.bottomButton, { width: 50 }]}
        onPress={() => onSave()}
      >
        <Icon ios="ios-save" android="md-save" />
      </Button>
    </View>
  );
};

const BallSummaryTable = ({ points }) => {
  // log({ points: points });

  const [loading, setLoading] = useState(true);
  const [forceUpdate, setForceUpdate] = useState(0);
  const [data, setData] = useState([]);
  useEffect(() => onLoad(), [forceUpdate, points]);

  // const data = [{ test: "test", test1: "test1" }];

  const onLoad = () => {
    const rangeMapper = [
      [0, 1, "Yorker"],
      [1, 2, "Full"],
      [2, 4, "Good"],
      [4, 5, "Short"],
    ];

    const groupedPoints = _.groupBy(points, function (point) {
      const json = point.json;
      return json["Length"];
    });

    //Code for automatic selection of Length
    // const test = points.map((p) => {
    //   log("p map");
    //   const json = JSON.parse(p.json);
    //   const pointY = json["pointY"] * parseFloat(SUMMARY_ROTATEVIEW_SCALE);

    //   rangeMapper.map((mVal) => {
    //     log(yValues[mVal[0]], pointY, yValues[mVal[1]]);
    //     if (pointY >= yValues[mVal[0]] && pointY < yValues[mVal[1]]) {
    //       log("if 1");
    //       if (groupedPoints.hasOwnProperty(mVal[2])) {
    //         groupedPoints[mVal[2]].push(p);
    //         log("if 2");
    //       } else {
    //         log("else 2");
    //         groupedPoints[mVal[2]] = [p];
    //       }
    //     }
    //   });
    // });

    // log({ groupedPoints });

    const headings = [...rangeMapper.map((m) => m[2])];

    const sideHeadings = [
      "Total",
      "%",
      "Length",
      "Line",
      "L&L",
      "Error",
      "Speed",
      "pointY",
    ];

    const data = sideHeadings.map((sh, ish) => {
      const row = { "": sh };
      headings.map((h, i) => {
        const fpoints = groupedPoints[h] || [];

        if (sh == "Total") {
          // log({ fpoints }, sh, h);
          row[h] = fpoints.length;
        } else if (sh == "%") {
          row[h] = to2DecimalPlaces((fpoints.length / points.length) * 100);
        } else if (["Length", "Line", "L&L", "Error"].includes(sh)) {
          row[h] = fpoints.filter((point) => {
            const json = point.json;
            return json["Quality"] == sh;
          }).length;
        } else if (["Speed"].includes(sh)) {
          row[h] = to2DecimalPlaces(
            _.meanBy(
              fpoints.filter((p) => {
                return !isNaN(p.json[sh]);
              }),
              function (point) {
                const json = point.json;
                return Number(json[sh]);
              }
            )
          );
        } else if (["pointY"].includes(sh)) {
          row[h] = to2DecimalPlaces(
            _.meanBy(
              fpoints.filter((p) => {
                return !isNaN(p.json[sh]);
              }),
              function (point) {
                const json = point.json;
                return getYDistanceFromPointY(
                  json[sh],
                  SUMMARY_ROTATEVIEW_SCALE
                );
              }
            )
          );
        }
      });
      row["Total"] = ["Speed", "pointY"].includes(sh)
        ? to2DecimalPlaces(_.mean(Object.values(row).filter((n) => !isNaN(n))))
        : to1DecimalPlaces(_.sum(Object.values(row).filter((n) => !isNaN(n))));
      return row;
    });

    // log(data);
    setLoading(false);
    setData(data);
  };

  if (loading) {
    return <ActivityIndicator />;
  }

  return (
    <>
      {DEVELOPER_MODE && (
        <Button onPress={() => setForceUpdate(forceUpdate + 1)}>
          <Text>force update</Text>
        </Button>
      )}
      <DataGrid
        showBorder={true}
        data={data}
        showHeader={true}
        key={"datagriddafdsf"}
      />
    </>
  );
};
