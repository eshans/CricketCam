import { Toast, Icon, Right } from "native-base";
import RNEButton from "react-native-elements/dist/buttons/Button";
import React, { useState } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  Pressable,
  View,
  TextInput,
  Dimensions,
  ScrollView,
} from "react-native";
import { ADMIN_PASSWORD, DEVELOPER_MODE, STYLES, TOAST_PROPS } from "../constants/values";
import { addParticipant } from "./database/participants";
import DeveloperDialog from "./devoloperDialog";

const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
    modalView: {
      margin: 20,
      backgroundColor: "white",
      borderRadius: 5,
      padding: 10,
      // alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
    },
    scrollView: {
      flexGrow: 0,
    },
    button: {
      borderRadius: 5,
      padding: 10,
      elevation: 2,
    },
    buttonOpen: {
      backgroundColor: "black",
    },
    buttonClose: {
      backgroundColor: "black",
      margin: 5,
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center",
    },
    modalText: {
      margin: 10,
      textAlign: "center",
    },
  });

export const AdvancedControlsButton = ({ screen, ...props }) => {
  const [modalVisible, setModalVisible] = useState(false);

  return (
      <>
      <RNEButton
            title="Advanced"
            icon={{ name: "settings", color: "white" }}
            buttonStyle={STYLES.baseButton}
            onPress={() => setModalVisible(!modalVisible)}
          />
    <View
      style={{
        alignItems: "right",
        position: "absolute",
        width: 80,
        height: 100,
        zIndex: 20000,
        top: 0,
        right: 0,
      }}
    >
        
        {/* <Icon
            name="help-circle"
            onPress={() => setModalVisible(!modalVisible)}
            onLongPress={() => {setDeveloperMode(false)}}
            style={{ color: "red", margin: 20 }}
          ></Icon> */}
      <View style={styles.centeredView}>
        <AdvancedControlsDialog
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          screen={screen}
          state={props.state}
          setState={props.setState}
        />
      </View>
    </View>
    </>
  );
};

export const AdvancedControlsDialog = ({ modalVisible, setModalVisible, screen }) => {
  // rconsole.log({ modalVisible });
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
        setModalVisible(!modalVisible);
      }}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>Advanced Grid Settings</Text>
          <ScrollView style={styles.scrollView}>
            <Text style={styles.modalText}>
              Work in progess
            </Text>
          </ScrollView>
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => setModalVisible(!modalVisible)}
          >
            <Text style={styles.textStyle}>Close</Text>
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};
