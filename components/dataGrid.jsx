// import React in our code
import React, { useEffect, useState } from "react";

// import all the components we are going to use
import {
  SafeAreaView,
  StyleSheet,
  View,
  FlatList,
  Image,
  Text,
  Dimensions,
  ScrollView,
  Pressable,
} from "react-native";
import { CheckBox } from "react-native-elements/dist/checkbox/CheckBox";
import { excelExport } from "./exportExcel";
import { Button } from "native-base";

const DataGrid = React.memo(
  ({
    data,
    showHeader,
    showBorder,
    containerStyles = {},
    textStyles = {},
    selectable = false,
    exportable = false,
    exportFileName = "export",
    deletable = false,
    deleteFunction = () => console.log("no delete function passed as props"),
    selected, 
    setSelected,
    HighLightRowId
  }) => {
    const [dataSource, setDataSource] = useState([]);
    
    const [forceRef, setForceRef] = useState(false);

    const forceRefresh = () => {
      setForceRef(!forceRef);
    };

    // console.log("datagrid data ", data, dataSource);

    const selectOne = (id) => {
      const newSelected = selected;
      newSelected[id] = !(selected.hasOwnProperty(id) && selected[id]);
      console.log("selected one ", id, selected, newSelected);
      setSelected(newSelected);
      forceRefresh();
    };

    const selectAll = () => {
      console.log("select all");
      const notAllSelected = data
        .map((item) => {
          return !selected.hasOwnProperty(item.id) || !selected[item.id];
        })
        .filter((item) => item)[0];

      // console.log(
      //   data.map((item) => {
      //     return !selected.hasOwnProperty(item.id) || !selected[item.id];
      //   }).filter(item => item)
      // );
      if (notAllSelected) {
        const newSelected = selected;
        data.map((item) => {
          newSelected[item.id] = true;

          // console.log("selectall ", { selected, newSelected });
        });
        console.log({ newSelected });
        setSelected(newSelected);
      } else {
        const newSelected = selected;
        data.map((item) => {
          newSelected[item.id] = false;

          // console.log("deselect ", { selected }, item.id);
        });
        console.log({ newSelected });
        setSelected(newSelected);
      }
      forceRefresh();
    };

    const numberOfCols = data
      ? selectable
        ? Object.keys(data[0]).length + 1
        : Object.keys(data[0]).length
      : 0;

    useEffect(() => {
      const items = []; 
      if (showHeader && data) {
        if (selectable) {
          items.push(
            <CheckBox
              key={`checkbox-all`}
              checked={
                !data
                  .map(
                    (item) =>
                      !selected.hasOwnProperty(item.id) || !selected[item.id]
                  )
                  .filter((item) => item)[0]
              }
              onPress={selectAll}
              style={{ padding: 0, border: 0, margin: 0, height: 0}}
            />
          );
        }
        Object.keys(data[0]).map((key) => {
          items.push(
            <Text
              key={`item-${key}`}
              style={{
                padding: 5,
                ...textStyles,
                fontWeight: "bold",
              }}
            >
              {key}
            </Text>
          );
        });
      }
      if (data) {
        data.map((item) => {
          if (selectable) {
            items.push(
              <CheckBox
                key={`checkbox-${item.id}`}
                checked={selected.hasOwnProperty(item.id) && selected[item.id]}
                onPress={() => selectOne(item.id)}
              />
            );
          }
          Object.values(item).map((key) => items.push(key));
        });
        setDataSource(items);
      }
    }, [data, selectable, selected, forceRef]);

    return (
      <SafeAreaView
        style={{
          ...styles.container,
          ...containerStyles,
        }}
        key="fdsafdksa"
      >
        {/* <ScrollView style={styles.scrollView}> */}
        {showHeader && data && (
          <FlatList
            key={"fdosagfdsgpnf"}
            data={dataSource.splice(0, numberOfCols)}
            style={{
              padding: 0,
              borderWidth: 1,
              borderColor: "rgb(100, 100, 200)",
              borderRadius: 5,
              height: selectable ? null : null,
              backgroundColor: "rgba(230, 230, 240, 1)",
              maxHeight: 50
            }}
            renderItem={({ item }) => (
              <View
                key={item}
                style={{
                  flex: 1,
                  flexDirection: "column",
                  margin: 1,
                  borderWidth: showBorder ? 0.5 : 0,
                  borderColor: showBorder ? "rgb(100, 100, 200)" : "white",
                }}
              >
                {React.isValidElement(item) ? (
                  item
                ) : (
                  <Text
                    key={`item-${item}`}
                    style={{ padding: 5, ...textStyles }}
                  >
                    {item}
                  </Text>
                )}
              </View>
            )}
            //Setting the number of column
            numColumns={numberOfCols}
            keyExtractor={(item, index) => index}
          />
        )}
        {data && (
          <FlatList
            key={"fdosapnf"}
            data={showHeader ? dataSource : dataSource}
            style={{
              maxHeight: Dimensions.get("window").height / 2,
              padding: 0,
              borderWidth: 1,
              borderColor: "rgb(100, 100, 200)",
              borderRadius: 5,
              
            }}
            renderItem={({ item }) => (
              <View
                key={item}
                style={{
                  flex: 1,
                  flexDirection: "column",
                  margin: 0,
                  borderWidth: showBorder ? 0.3 : 0,
                  borderColor: showBorder ? "rgba(100, 100, 200, 0.3)" : "white",
                  backgroundColor:  HighLightRowId && HighLightRowId == item && "rgba(0, 0, 255, 0.5)"
                }}
              >
                {React.isValidElement(item) ? (
                  item
                ) : (
                  <Text
                    key={`item-${item}`}
                    style={{ padding: 8, ...textStyles }}
                  >
                    {item}
                  </Text>
                )}
              </View>
            )}
            //Setting the number of column
            numColumns={
              selectable
                ? Object.keys(data[0]).length + 1
                : Object.keys(data[0]).length
            }
            keyExtractor={(item, index) => index}
          />
        )}
        {(deletable || exportable) &&
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            // backgroundColor: "white",
            marginBottom: 100,
            margin: 20,
          }}
          key={"datagrid-view3"}
        >
          {exportable && (
            <Button
              disabled={!Object.values(selected).filter((item) => item)[0]}
              style={{ margin: 5, padding: 5 }}
              onPress={() =>
                excelExport(
                  data.filter(
                    (item) =>
                      selected.hasOwnProperty(item.id) && selected[item.id]
                  ),
                  exportFileName
                )
              }
            >
              <Text>Export Selected</Text>
            </Button>
          )}
          {deletable && (
            <Button
              style={{ margin: 5, padding: 5 }}
              disabled={!Object.values(selected).filter((item) => item)[0]}
              onPress={() =>
                deleteFunction(
                  data
                    .filter(
                      (item) =>
                        selected.hasOwnProperty(item.id) && selected[item.id]
                    )
                    .map((item) => item.id)
                )
              }
            >
              <Text>Delete Selected</Text>
            </Button>
          )}
        </View>
  }
        {/* </ScrollView> */}
      </SafeAreaView>
    );
  }
);
export default DataGrid;

const styles = StyleSheet.create({
  container: {
    zIndex: 1000,
    flex: 1,
    justifyContent: "center",
    // backgroundColor: "white",
  },
  imageThumbnail: {
    justifyContent: "center",
    alignItems: "center",
    height: 100,
  },
});
