import React, { useEffect, useState } from "react";
import {
  PanResponder,
  View,
  Image,
  Dimensions,
  Text,
  ScrollView,
  TextInput,
  SafeAreaView,
  Select,
  Alert,
  Pressable,
  TouchableOpacity,
} from "react-native";
import {
  Icon,
  // Picker,
  Button
} from "native-base";
import Picker from "react-native-simple-modal-picker";
import { Svg, Line, Circle } from "react-native-svg";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { getParticipants } from "./database/participants";
import {
  DEVELOPER_MODE,
  DISABLED_COLOR,
  IPHONE_MODE,
  PRIMARY_COLOR,
  STYLES,
} from "../constants/values";
import AddParticipantButton from "./AddParticipantDialog";
import RNEButton from "react-native-elements/dist/buttons/Button";
import { Tooltip } from "react-native-elements";
import { AdvancedControlsButton } from "./AdvancedGridControls";
import { log } from "../constants/utils";
import ModalSelector from "react-native-modal-selector";
import { deleteParticipantAlert } from "./database/participants";

const styles = {
  topActions: {
    marginRight: 100,
    marginLeft: 100,
    flexDirection: "row",
    // justifyContent: "space-between",
    backgroundColor: "transparent",
    zIndex: 1000,
  },
  controlContainer: {
    padding: 20,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonContainer: {
    flex: 0,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  numberInput: STYLES.input,
};

const ControlSurface = ({ title, value, onPress, onReset, ...props }) => {
  return (
    <View style={styles.controlContainer}>
      
    </View>
  );
};

export const GridControls = ({
  onChangeValue,
  onReset,
  state,
  setState,
  onStateReset,
  ...props
}) => {

  const SideTableButton = ({ field, value }) => {
    return (
      <Button
        // key={`${field}=${value}`}
        style={{
          margin: 2,
          padding: 5,
          backgroundColor:
            state[field] === value ? PRIMARY_COLOR : DISABLED_COLOR,
          height: 30,
        }}
        onPress={() => {
          const newState = state;
          newState[field] = value;
          setState(newState);
          setState({ ...state, update: Math.random() });
          // console.log('values set ',value)
        }}
      >
        <Text
        // key="dsjnfdsaf"
        >
          {value}
        </Text>
      </Button>
    );
  };
  // log({ state });
  const [participants, setParticipantsList] = useState(state.participants);

  const setParticipants = (newVal) => {
    // log({
    //   ...state,
    //   participants: [...state.participants, { id: "new", name: newVal }],
    // });
    setState({
      ...state,
      participants: [...state.participants, { id: "new", name: newVal }],
    });
    setParticipantsList([...state.participants, { id: "new", name: newVal }]);
  };

  const setParticipantInRotateView = (newVal) => {
    setState({ ...state, participant: newVal });
  };

  const testing = IPHONE_MODE;

  log("SHOW grid controls");

  let index = 0;
  const data = [
    { key: index++, section: true, label: "Fruits" },
    { key: index++, label: "Red Apples" },
    { key: index++, label: "Cherries" },
    {
      key: index++,
      label: "Cranberries",
      accessibilityLabel: "Tap here for cranberries",
    },
    // etc...
    // Can also add additional custom keys which are passed to the onChange callback
    { key: index++, label: "Vegetable", customKey: "Not a fruit" },
  ];
  return (
    <>
      {DEVELOPER_MODE && (
        <Tooltip
          popover={
            <View>
              <Text>{JSON.stringify(state, null, 2)}</Text>
            </View>
          }
          height={400}
          widht={400}
        >
          <Text>!View State</Text>
        </Tooltip>
      )}
      {/* {!testing ? (
        <>
          <View style={styles.topActions}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            {["UL", "UR", "DL", "DR", 'MS'].map((val) => {
              return <SideTableButton key={val} field={"selectedPoint"} value={val} />;
            })}
          </View>
            
          </View>
          <View style={styles.topActions}>
            
          </View>
        </>
      ) : null} */}
      <View style={styles.topActions}>
        {/* {!testing ? (
          <>
            <View style={styles.controlContainer}>
              <TouchableWithoutFeedback
                onPress={() => {
                  onReset("XGrids");
                  onReset("YGrids");
                }}
              >
                <Text>Grid Divisions</Text>
              </TouchableWithoutFeedback>
              <View svaluestyle={styles.buttonContainer}>
                <Icon
                  title="button"
                  name="add-circle-sharp"
                  onPress={() => onChangeValue("XGrids", "increment")}
                ></Icon>
                <Text>X</Text>

                <Icon
                  filled
                  title="button"
                  name="remove-circle"
                  onPress={() => onChangeValue("XGrids", "decrement")}
                ></Icon>
              </View>
              <View style={styles.buttonContainer}>
                <Icon
                  title="button"
                  name="add-circle-outline"
                  onPress={() => onChangeValue("YGrids", "increment")}
                ></Icon>
                <Text>Y</Text>

                <Icon
                  filled
                  title="button"
                  name="remove-circle-outline"
                  onPress={() => onChangeValue("YGrids", "decrement")}
                ></Icon>
              </View>
            </View>

            <ControlSurface
              onPress={onChangeValue}
              onReset={onReset}
              title={"Width"}
              value={"width"}
              fineAdjustment={true}
            />
          </>
        ) : null} */}
        {/* <ControlSurface
          onPress={onChangeValue}
          onReset={onReset}
          title={"Height"}
          value={"height"}
          fineAdjustment={true}
        /> */}
        {/* <View style={styles.controlContainer}>
          <AdvancedControlsButton />
        </View> */}
        {props?.screen == "addPoint" ? (
          <View style={styles.controlContainer}>
            <RNEButton
              title="Save state"
              icon={{ name: "save", color: "white" }}
              buttonStyle={STYLES.baseButton}
              onPress={() => props.onSaveStateToExistingVideo()}
            />
          </View>
        ) : (
          <View style={styles.controlContainer}>
            <TouchableWithoutFeedback
              // onPress={() => {
              //   setState({ ...state, participant: null });
              // }}
              style={{ width: 200 }}
            >
              <Text>Participant</Text>
            </TouchableWithoutFeedback>

            <View style={styles.buttonContainer}>
              <ModalSelector
              style={{width: 150}}
                data={
                  participants &&
                  participants.map((val) => ({
                    key: val.id,
                    label: val.name,
                    // value :val.name
                  }))
                }
                initValue={state.participant}
                onChange={(val) => setState({ ...state, participant: val.label })}
              />

              {/* <Picker
                style={[STYLES.input, { width: 200 }]}
                enabled={true}
                selectedValue={state.participant}
                mode="dialog"
                onValueChange={(val, index) =>
                  setState({ ...state, participant: val })
                }
              >
                {participants &&
                  participants.map((val) => (
                    <Picker.Item
                      key={val.id}
                      label={val.name}
                      value={val.name}
                    />
                  ))}
              </Picker> */}
              <AddParticipantButton
                isIcon={true}
                setParticipants={setParticipants}
                setParticipantInRotateView={setParticipantInRotateView}
              />
              <View style={{ padding: 10 }}>
                <TouchableOpacity
                  disabled={!state.participant}
                  onPress={() => {
                    deleteParticipantAlert(`${state.participant}`);
                  }}
                >
                  <Icon name="trash-outline"></Icon>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        )}

        {/* <View style={styles.controlContainer}>
          
          <RNEButton
            title="reset"
            icon={{ name: "refresh", color: "white" }}
            buttonStyle={STYLES.baseButton}
            onPress={() => onStateReset()}
          />
        </View> */}
        <View style={styles.buttonContainer}>
            <TextInput
              style={styles.numberInput}
              onChangeText={(e) => {
                state.actualLength = e;
              }}
              editable={true}
              // onBlur={(e) => setState({ ...state, actualWidth: e })}
              value={state.actualLength}
              placeholder="Actual Length"
              keyboardType="numeric"
            />
          </View>
          <View style={styles.buttonContainer}>
            <TextInput
              style={styles.numberInput}
              onChangeText={(e) => {
                state.actualWidth = e;
              }}
              editable={true}
              // onBlur={(e) => setState({ ...state, actualWidth: e })}
              value={state.actualWidth}
              placeholder="Actual Width"
              keyboardType="numeric"
            />
          </View>

        {/* <View style={styles.controlContainer}>
          <TouchableWithoutFeedback
            onPress={() => {
              onReset("XSubGrids");
              onReset("YSubGrids");
            }}
          >
            <Text>Grid Sub Divisions</Text>
          </TouchableWithoutFeedback>
          <View style={styles.buttonContainer}>
            <Icon
              title="button"
              name="add-circle-sharp"
              onPress={() => onChangeValue("XSubGrids", "increment")}
            ></Icon>
            <Text>X</Text>

            <Icon
              filled
              title="button"
              name="remove-circle"
              onPress={() => onChangeValue("XSubGrids", "decrement")}
            ></Icon>
          </View>
          <View style={styles.buttonContainer}>
            <Icon
              title="button"
              name="add-circle-outline"
              onPress={() => onChangeValue("YSubGrids", "increment")}
            ></Icon>
            <Text>Y</Text>

            <Icon
              filled
              title="button"
              name="remove-circle-outline"
              onPress={() => onChangeValue("YSubGrids", "decrement")}
            ></Icon>
          </View>
        </View> */}
      </View>
      <View style={styles.topActions}>
        {/* <View style={styles.controlContainer}>
          <TouchableWithoutFeedback
            onPress={() => {
              setState({ ...state, actualLength: null });
            }}
            disabled={true}
          >
            <Text>Actual Length</Text>
          </TouchableWithoutFeedback>

          <View style={styles.buttonContainer}>
            <TextInput
              style={styles.numberInput}
              onChangeText={(e) => {
                state.actualLength = e;
              }}
              editable={false}
              // onBlur={(e) => setState({ ...state, actualWidth: e })}
              value={state.actualLength}
              placeholder="Length"
              keyboardType="numeric"
            />
          </View>
        </View>
        <View style={styles.controlContainer}>
          <TouchableWithoutFeedback
            onPress={() => {
              setState({ ...state, actualWidth: null });
            }}
            disabled={true}
          >
            <Text>Actual Width</Text>
          </TouchableWithoutFeedback>
          <View style={styles.buttonContainer}>
            <TextInput
              style={styles.numberInput}
              onChangeText={(e) => {
                state.actualWidth = e;
              }}
              value={state.actualWidth}
              placeholder="Width"
              keyboardType="numeric"
              editable={false}
            />
          </View>
        </View> */}
        {/* <View style={styles.controlContainer}>
          <TouchableWithoutFeedback
          // onPress={() => {
          //   setState({ ...state, participant: null });
          // }}
          >
            <Text>Participant</Text>
          </TouchableWithoutFeedback>

          <View style={styles.buttonContainer}>
            <Picker
              style={STYLES.input}
              enabled={true}
              selectedValue={state.participant}
              mode="dialog"
              onValueChange={(val, index) =>
                setState({ ...state, participant: val })
              }
            >
              {participants &&
                participants.map((val) => (
                  <Picker.Item key={val.id} label={val.name} value={val.name} />
                ))}
            </Picker>
            <AddParticipantButton
              isIcon={true}
              setParticipants={setParticipants}
              setParticipantInRotateView={setParticipantInRotateView}
            />
          </View>
        </View> */}
      </View>
      {/* <View style={styles.topActions}>
      <View style={styles.controlContainer}>
        <View style={styles.buttonContainer}>
          <Text>
            {`Y distance: ${
              (state.actualLength * (state.height - state.pointY)) /
              state.height
            } | `}
          </Text>
          <Text>
            {`X distance: ${
              (state.actualWidth * (state.width - state.pointX)) / state.width
            } `}
          </Text>
        </View>
      </View>
          </View> */}
    </>
  );
};
