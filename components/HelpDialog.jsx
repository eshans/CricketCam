import { Toast, Icon, Right } from "native-base";
import React, { useState } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  Pressable,
  View,
  TextInput,
  Dimensions,
  ScrollView,
} from "react-native";
import { ADMIN_PASSWORD, DEVELOPER_MODE, STYLES, TOAST_PROPS } from "../constants/values";
import { addParticipant } from "./database/participants";
import DeveloperDialog from "./devoloperDialog";

const PAGE_HELP_CONTENT = {
  videoRecorder: `This is videoRecorder help text`,
  RecordGridControls: `this is RecordGridControls help text`,
  videoPlayer: `this is videoPlayer help text`,
  videosList: "this is videosList help text",
  summaryView: `this is summaryView help text`,
};

const HelpButton = ({ screen }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [adminModalVisible, setAdminModalVisible] = useState(false);
  const [developerMode, setDeveloperMode] = useState(DEVELOPER_MODE);

  return (
    <View
      style={{
        alignItems: "right",
        position: "absolute",
        width: 80,
        height: 100,
        zIndex: 20000,
        top: 0,
        right: 0,
      }}
    >
      {developerMode ? (
        <>
          <Icon
            name="help-circle"
            onPress={() => setModalVisible(!modalVisible)}
            onLongPress={() => {setDeveloperMode(false)}}
            style={{ color: "red", margin: 20 }}
          ></Icon>
          <View style={styles.centeredView}>
            <DeveloperDialog
              modalVisible={modalVisible}
              setModalVisible={setModalVisible}
            />
          </View>
        </>
      ) : (
        <>
          <Icon
            name="help-circle"
            onPress={() => setModalVisible(!modalVisible)}
            onLongPress={() => setAdminModalVisible(true)}
            style={{ color: "blue", margin: 20 }}
          ></Icon>
          <View style={styles.centeredView}>
            <HelpDialog
              modalVisible={modalVisible}
              setModalVisible={setModalVisible}
              screen={screen}
            />
            <AdminDialog
              modalVisible={adminModalVisible}
              setModalVisible={setAdminModalVisible}
              setDeveloperMode={setDeveloperMode}
            />
          </View>
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 5,
    padding: 10,
    // alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  scrollView: {
    flexGrow: 0,
  },
  button: {
    borderRadius: 5,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "black",
  },
  buttonClose: {
    backgroundColor: "black",
    margin: 5,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    margin: 10,
    textAlign: "center",
  },
});

export default HelpButton;

export const HelpDialog = ({ modalVisible, setModalVisible, screen }) => {
  // rconsole.log({ modalVisible });
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
        setModalVisible(!modalVisible);
      }}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>Help</Text>
          <ScrollView style={styles.scrollView}>
            <Text style={styles.modalText}>
              {PAGE_HELP_CONTENT.hasOwnProperty(screen)
                ? PAGE_HELP_CONTENT[screen]
                : `No help text for this screen 
(current screen: ${screen})`}
            </Text>
          </ScrollView>
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => setModalVisible(!modalVisible)}
          >
            <Text style={styles.textStyle}>Close</Text>
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};

const AdminDialog = ({ modalVisible, setModalVisible, setDeveloperMode }) => {

  let password = ''
  // rconsole.log({ modalVisible });
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
        setModalVisible(!modalVisible);
      }}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>Enter developer mode</Text>
          <ScrollView style={styles.scrollView}>
            <TextInput placeholder={'password'} style={STYLES.input} onChangeText={(text) => password=text}></TextInput>
          </ScrollView>
          <Pressable
            style={[styles.button, styles.buttonClose, { color: "red"}]}
            onPress={() => {
              if(password == ADMIN_PASSWORD) {
                setDeveloperMode(true)
              }
              else {
                Toast.show({
                  text: `Wrong password!`,
                  ...TOAST_PROPS("error"),
                });
              }
              setModalVisible(!modalVisible)
              
            }}
          >
            <Text style={styles.textStyle}>Enter</Text>
          </Pressable>
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => setModalVisible(!modalVisible)}
          >
            <Text style={styles.textStyle}>Cancel</Text>
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};
