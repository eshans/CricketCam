// Example: Example of SQLite Database in React Native
// https://aboutreact.com/example-of-sqlite-database-in-react-native

import React, { useEffect } from "react";
import { View, Text, SafeAreaView } from "react-native";
import Mybutton from "./components/Mybutton";
import Mytext from "./components/Mytext";
// import { openDatabase } from 'react-native-sqlite-storage';

import * as SQLite from "expo-sqlite";

export const db = SQLite.openDatabase("db.db");

const tableDetails = `(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))`;

const HomeScreen = ({ navigation }) => {
  const [tables,setTables] = React.useState([])
  const table = "participant";

  useEffect(() => {
    db.transaction(function (txn) {
        txn.executeSql(
          `SELECT name FROM sqlite_master WHERE type='table'`,
          [],
          function (tx, res) {
            console.log(res.rows._array)
            setTables(res.rows._array.map(m => m.name))
          }
        );
      txn.executeSql(
        `SELECT name FROM sqlite_master WHERE type='table' AND name=${table}`,
        [],
        function (tx, res) {
          console.log("item:", res.rows.length);
          if (res.rows.length == 0) {
            txn.executeSql(`DROP TABLE IF EXISTS ${table}${tableDetails}`, []);
            txn.executeSql(`CREATE TABLE IF NOT EXISTS ${table}`, []);
          }
        }
      );
    });
    
  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: "white" }}>
        {tables.map((table) => (
          <View style={{ flex: 1 }} key={table}>
            <Mytext text={`${table}`} />
            {/* <Mybutton
            title="Register"
            customClick={() => navigation.navigate('Register')}
          /> */}
            {/* <Mybutton
            title="Update"
            customClick={() => navigation.navigate('Update')}
          /> */}
            {/* <Mybutton
            title="View"
            customClick={() => navigation.navigate('View')}
          /> */}
            <Mybutton
              title="View All"
              customClick={() => navigation.navigate("ViewAll", {table})}
            />
            {/* <Mybutton
            title="Delete"
            customClick={() => navigation.navigate('Delete')}
          /> */}
          </View>
        ))}

        <Text
          style={{
            fontSize: 18,
            textAlign: "center",
            color: "grey",
          }}
        >
          Example of SQLite Database in React Native
        </Text>
        <Text
          style={{
            fontSize: 16,
            textAlign: "center",
            color: "grey",
          }}
        >
          www.aboutreact.com
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default HomeScreen;
