// Example: Example of SQLite Database in React Native
// https://aboutreact.com/example-of-sqlite-database-in-react-native
// Screen to view all the user*/

import React, { useState, useEffect } from "react";
import { FlatList, Text, View, SafeAreaView } from "react-native";
// import { openDatabase } from 'react-native-sqlite-storage';

import { db } from "./HomeScreen";

const ViewAllUser = ({navigation, route}) => {
  const [flatListItems, setFlatListItems] = useState([]);
  const [fields, setFields] = useState([]);

const navigate = navigation.navigate('test')

  const table = route.params.table;

  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql(`SELECT * FROM ${table}`, [], (tx, { rows}) => {
        var temp = [];
        // for (let i = 0; i < results.rows.length; ++i)
        //   temp.push(results.rows.item(i));
        setFlatListItems(rows._array);
        setFields(Object.keys(rows._array[0]));
        console.log(rows._array)
      });
    });
    console.log(fields);
  }, []);

  let listViewItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.2,
          width: "100%",
          backgroundColor: "#808080",
        }}
      />
    );
  };

  let listItemView = (item) => {
    console.log(item);
    return (
      <View
        key={item.user_id}
        style={{ backgroundColor: "white", padding: 20 }}
      >
        {fields.map((f) => {
          if (f == "json") {

            const json = JSON.parse(item[f]);
            // const labels = Object.keys(json);
            // console.log(json, item[f]);
            return (
              <>
                <Text>json</Text>
                <View style={{ paddingLeft: 20 }}>
                  {Object.keys(json).map((l) => {
                    console.log(l, json[l])
                    return <Text key={`${json[l]}${l}`}>{`${l}: ${json[l]}`}</Text>;
                  })}
                </View>
              </>
            );
          } else {
            return <Text key={`${f}: ${item[f]}`}>{`${f}: ${item[f]}`}</Text>;
          }
        })}
      </View>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <View style={{ flex: 1 }}>
          <FlatList
            data={flatListItems}
            ItemSeparatorComponent={listViewItemSeparator}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => listItemView(item)}
          />
        </View>
        <Text
          style={{
            fontSize: 18,
            textAlign: "center",
            color: "grey",
          }}
        >
          Example of SQLite Database in React Native
        </Text>
        <Text
          style={{
            fontSize: 16,
            textAlign: "center",
            color: "grey",
          }}
        >
          www.aboutreact.com
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default ViewAllUser;
