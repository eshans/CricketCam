import React, { useState } from "react";
import * as MediaLibrary from "expo-media-library";

import { Toast, Icon, Right } from "native-base";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  Pressable,
  View,
  TextInput,
  Dimensions,
  ScrollView,
} from "react-native";
import { STYLES, TOAST_PROPS} from "../constants/values";

export const exportToMediaLibrary = (localUri, hasPermission) => {
  console.log("exportToMediaLibrary", localUri, hasPermission);
  if (hasPermission) {
    MediaLibrary.saveToLibraryAsync(localUri).then(() => {
      Toast.show({
        text: "Video exported to photos!",
        ...TOAST_PROPS('success'),
      });
    }).catch(e => {
      Toast.show({
        text: "Error exporting video",
        ...TOAST_PROPS('error'),
      });
      console.log(e)
    });
    
  }
  else {
    Toast.show({
      text: "Please give permission to export video",
      ...TOAST_PROPS('error'),
    });
  }
};

const ExportVideoToMediaLibraryButton = ({ localUri }) => {
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <View
    // style={{
    //   alignItems: "right",
    //   position: "absolute",
    //   width: 80,
    //   height: 100,
    //   zIndex: 20000,
    //   top: 0,
    //   right: 0,
    // }}
    >
      {/* <Icon
        name="help-circle"
        onPress={() => setModalVisible(!modalVisible)}
        style={{ color: "blue", margin: 20 }}
      ></Icon> */}
      <Icon name="add-outline" onPress={() => setModalVisible(!modalVisible)} />
      <View style={styles.centeredView}>
        <HelpDialog
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          localUri={localUri}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 5,
    padding: 10,
    // alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  scrollView: {
    flexGrow: 0,
  },
  button: {
    borderRadius: 5,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "black",
  },
  buttonClose: {
    backgroundColor: "black",
    margin: 5,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    margin: 10,
    textAlign: "center",
  },
});

export default ExportVideoToMediaLibraryButton;

export const HelpDialog = ({ modalVisible, setModalVisible, localUri }) => {
  const [hasPermission, setPermission] = useState(false);
  // console.log({ modalVisible });
  MediaLibrary.getPermissionsAsync().then((value) => {
    setPermission(
      value.accessPrivileges === "all" || value.accessPrivileges === "limited"
    );
  });
  if (!hasPermission) {
    // console.log({ "media permissions": localUri });
    MediaLibrary.requestPermissionsAsync();
  }
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
        setModalVisible(!modalVisible);
      }}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>Export video to photos</Text>
          <ScrollView style={styles.scrollView}>
            <Text style={styles.modalText}></Text>
          </ScrollView>
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => setModalVisible(!modalVisible)}
          >
            <Text style={styles.textStyle}>Cancel</Text>
          </Pressable>
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => 
              {
              exportToMediaLibrary(localUri, hasPermission, setPermission);
              setModalVisible(!modalVisible)
              }
            }
          >
            <Text style={styles.textStyle}>Add</Text>
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};
