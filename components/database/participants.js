import { Toast } from "native-base";
import React from "react";
import { log } from "../../constants/utils";
import { TOAST_PROPS } from "../../constants/values";
import { openDatabase, catchError, forceUpdate } from "./database";

export function getParticipants(setParticipants, filterVal) {
  const db = openDatabase();
  // const [participants, setParticipants] = React.useState(null);
  // let participants;
  log("getParticipants");
  log("filter ", filterVal);

  // React.useEffect(() => {
  if (filterVal) {
    db.transaction((tx) => {
      tx.executeSql(
        `select * from participants where name = ?;`,
        [filterVal],
        (_, { rows }) => {
          // log("loaded participants ", JSON.stringify(rows._array));
          setParticipants(rows._array);
        },
        catchError
      );
    });
  } else {
    db.transaction((tx) => {
      tx.executeSql(
        `select * from participants;`,
        null,
        (_, { rows}) => {
          // log("loaded participants ", JSON.stringify(rows._array), rows);
          setParticipants(rows._array);
        },
        catchError
      );
    });
  }

  // }, []);

  // if (participants === null || participants.length === 0) {
  //   return null;
  // }
  // return participants;
}

export const addParticipant = (name, setParticipants, json = {}) => {
  const db = openDatabase();
  // is text empty?
  if (name === null || name === "") {
    log('ERROR', 'paricipant add name not defined')
    return false;
  }

  log("add participant to database");

  db.transaction(
    (tx) => {
      tx.executeSql(
        "insert into participants (name, json) values (?, ?);",
        [name, JSON.stringify(json)],
        () => {
          Toast.show({
            text: "Participant added",
            ...TOAST_PROPS('success'),
          });
        },
        (arg1, arg2) => {
          catchError(arg1, arg2);
          if (
            arg2
              .toString()
              .includes("UNIQUE constraint failed: participants.name")
          ) {
            Toast.show({
              text: "Error: Participant already exists",
              ...TOAST_PROPS('error'),
            });
          }
        }
      );
      tx.executeSql(
        "select * from participants;",
        [],
        (_, { rows }) => {
          // log("database ", JSON.stringify(rows));
          if (setParticipants) {
            setParticipants(rows);
          }
        },
        catchError
      );
    },
    null,
    forceUpdate
  );
  log("done add participant");
};

// const updateParticipant = (id) => {
//   const db = openDatabase();
//   db.transaction(
//     (tx) => {
//       tx.executeSql(`update participants set done = 1 where id = ?;`, [id]);
//     },
//     null,
//     forceUpdate
//   );
// };

export const deleteParticipant = (value) => {
  const db = openDatabase();
  db.transaction(
    (tx) => {
      tx.executeSql(`delete from participants where name = ?;`,
      [value],
      () => log("delete participant"),
      catchError);
    },
    null,
    forceUpdate
  );
};

export const changeParticipantDetails = (participant, details) => {
  const db = openDatabase();
  db.transaction(
    (tx) => {
      tx.executeSql(`update participants set json=? where name=?;`, [JSON.stringify({details}) ,participant], null, catchError);
    },
    null,
    forceUpdate
  );
}

export const getParticipantDetails = (participant, setPDetails) => {
  const db = openDatabase();
  db.transaction((tx) => {
    tx.executeSql(
      `select * from participants where name = ?;`,
      [participant],
      (_, { rows }) => {
        // log("loaded participants ", JSON.stringify(rows._array), JSON.parse(rows._array[0].json).details);
        setPDetails(JSON.parse(rows._array[0].json).details);
      },
      catchError
    );
  });

}

export const deleteParticipantAlert = (participant) =>
  Alert.alert(
    `Delete ${participant}`,
    "This action is not reversible. Note: this action will remove the participant name only from dropdown menu. If any videos are remaining, the participant will be shown",
    [
      {
        text: "Cancel",
        onPress: () => log("Cancel Pressed"),
        style: "cancel",
      },
      {
        text: "Delete",
        onPress: (participant) => deleteParticipant(participant),
      },
    ],
    { cancelable: true }
  );