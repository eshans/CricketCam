import React from "react";
import { openDatabase, catchError, forceUpdate } from "./database";
export function getPoints(setPoints, filterVals) {
  const db = openDatabase();
  // const [points, setPoints] = React.useState(null);
  // let points;
  console.log("getPoints");
  // console.log("filter ", filterVals);

  const points = [];

  const getFilterString = () => {
    var filterString = "";
    filterVals.map(
      (filterVal) =>
        (filterString =
          (filterString ? filterString + "," : "") + "'" + filterVal + "'")
    );
    return filterString;
  };

  const filterString = getFilterString();

  // console.log({ filterString: filterString });

  // React.useEffect(() => {
  db.transaction((tx) => {
    filterVals.map((filterVal, index) => {
      tx.executeSql(
        `select * from points where value = ?;`,
        [filterVal],
        (_, { rows: { _array } }) => {
          // console.log("loaded points 2 ", _array);
          points.push(
            ..._array.map((item) => ({
              ...item,
              json: JSON.parse(item.json),
            }))
          );
          console.log({ points });
          if (index + 1 === filterVals.length) {
            setPoints(points);
          }
        },
        catchError
      );
    });
  });

  // }, []);

  // if (points === null || points.length === 0) {
  //   return null;
  // }
  // return points;
}

export function getAllPoints(setPoints) {
  const db = openDatabase();
  // const [points, setPoints] = React.useState(null);
  // let points;
  console.log("getPoints");

  const points = [];

  // React.useEffect(() => {
  db.transaction((tx) => {
    tx.executeSql(
      `select * from points;`,
      null,
      (_, { rows: { _array } }) => {
        // console.log("loaded points 2 ", _array);
        points.push(
          ..._array.map((item) => ({
            ...item,
            json: JSON.parse(item.json),
          }))
        );
        setPoints(points);
      },
      catchError
    );
  });

  // }, []);

  // if (points === null || points.length === 0) {
  //   return null;
  // }
  // return points;
}

export const addPoint = (name, json) => {
  const db = openDatabase();
  // is text empty?
  if ((!json && name === null) || name === "") {
    return false;
  }

  console.log("add point to database");

  db.transaction(
    (tx) => {
      tx.executeSql(
        "insert into points (value, json) values (?, ?);",
        [name, JSON.stringify(json)],
        null,
        catchError
      );
      tx.executeSql(
        "select * from points;",
        [],
        (_, { rows }) => console.log("database "
        // , JSON.stringify(rows)
        ),
        catchError
      );
    },
    null,
    forceUpdate
  );
  console.log("done add point");
};

// const updatePoint = (id) => {
//   const db = openDatabase();
//   db.transaction(
//     (tx) => {
//       tx.executeSql(`update points set done = 1 where id = ?;`, [id]);
//     },
//     null,
//     forceUpdate
//   );
// };

export const deletePoint = (id) => {
  const db = openDatabase();
  db.transaction(
    (tx) => {
      tx.executeSql(`delete from points where id = ?;`, [id]);
    },
    null,
    forceUpdate
  );
};


export const clearAllPointsFromDatabase = () => {
  const db = openDatabase();
  db.transaction((tx) => {
    tx.executeSql(
      "drop table points;",
      [],
      () => {
        console.log("drop points table success");
      },
      catchError
    );
  });
  db.transaction((tx) => {
    tx.executeSql(
      "create table if not exists points (id integer primary key not null, value text, json text);",
      [],
      () => {
        console.log("table create success or already exist: points");
      },
      catchError
    );
  });
};