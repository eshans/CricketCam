import React from "react";
import { openDatabase, catchError, forceUpdate } from "./database";
export function getItems(setItems) {
  const db = openDatabase();
  // const [items, setItems] = React.useState(null);
  // let items;
  console.log("getItems");

  // React.useEffect(() => {
  db.transaction((tx) => {
    tx.executeSql(
      `select * from items;`,
      [],
      (_, { rows: { _array } }) => {
        // console.log("loaded items ", JSON.stringify(_array));
        setItems(
          _array.map((item) => ({
            ...item,
            json: JSON.parse(item.json),
          }))
        );
      },
      catchError
    );
  });
  // }, []);

  // if (items === null || items.length === 0) {
  //   return null;
  // }
  // return items;
}

export const addItem = (name, json) => {
  const db = openDatabase();
  // is text empty?
  if ((!json && name === null) || name === "") {
    return false;
  }

  console.log("add item to database");

  db.transaction(
    (tx) => {
      tx.executeSql(
        "insert into items (value, json) values (?, ?);",
        [name, JSON.stringify(json)],
        null,
        catchError
      );
      tx.executeSql(
        "select * from items;",
        [],
        (_, { rows }) =>
          console.log(
            "database "
            // , JSON.stringify(rows)
          ),
        catchError
      );
    },
    null,
    forceUpdate
  );
  console.log("done add item");
};

export const editItemJson = (name, json) => {
  console.log('video name ' ,name)
  const db = openDatabase();
  db.transaction((tx) => {
    tx.executeSql(
      `select * from items where value = "${name}";`,
      null,
      (_, { rows: { _array } }) => {
        console.log("edited video items ", _array);
      },
      catchError
    );
  });

  db.transaction(
    (tx) => {
      tx.executeSql(`update items set json = ? where value = "${name}";`, [
        JSON.stringify(json)]);
    },
    null,
    forceUpdate
  );
  console.log("done edit item json ", json);
};

// const updateItem = (id) => {
//   const db = openDatabase();
//   db.transaction(
//     (tx) => {
//       tx.executeSql(`update items set done = 1 where id = ?;`, [id]);
//     },
//     null,
//     forceUpdate
//   );
// };

export const deleteItem = (value) => {
  const db = openDatabase();
  db.transaction(
    (tx) => {
      tx.executeSql(
        `delete from items where value = ?;`,
        [value],
        () => console.log("delete item"),
        catchError
      );
    },
    null,
    forceUpdate
  );
};

export const clearAllItemsFromDatabase = () => {
  const db = openDatabase();
  db.transaction((tx) => {
    tx.executeSql(
      "drop table items;",
      [],
      () => {
        console.log("drop items table success");
      },
      catchError
    );
  });
  db.transaction((tx) => {
    tx.executeSql(
      "create table if not exists items (id integer primary key not null, value text, json text);",
      [],
      () => {
        console.log("table create success or already exist: items");
      },
      catchError
    );
  });
};
