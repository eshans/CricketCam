import React, { useState } from "react";
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Dimensions,
} from "react-native";
import Constants from "expo-constants";
import * as SQLite from "expo-sqlite";
import { log } from "../../constants/utils";

export function forceUpdate() {
  console.log("force update called");
}

export function openDatabase() {
  if (Platform.OS === "web") {
    return {
      transaction: () => {
        return {
          executeSql: () => {},
        };
      },
    };
  }

  const db = SQLite.openDatabase("db.db");
  console.log("database opened");
  return db;
}

const db = openDatabase();

export const createTableIfNotExist = () => {
  console.log("create table if exists");
  const db = openDatabase();
  // db.transaction((tx) => {
  //   tx.executeSql(
  //     "drop table items;",
  //     [],
  //     () => {
  //       console.log("drop items table success");
  //     },
  //     catchError
  //   );
  // });
  // db.transaction((tx) => {
  //   tx.executeSql(
  //     "drop table points;",
  //     [],
  //     () => {
  //       console.log("drop points table success");
  //     },
  //     catchError
  //   );
  // });
  // db.transaction((tx) => {
  //     tx.executeSql(
  //       "drop table participants;",
  //       [],
  //       () => {
  //         console.log("drop participants table success");
  //       },
  //       catchError
  //     );
  //   });
  db.transaction((tx) => {
    tx.executeSql(
      "create table if not exists items (id integer primary key not null, value text, json text);",
      [],
      () => {
        console.log("table create success or already exist: items");
      },
      catchError
    );
  });
  db.transaction((tx) => {
    tx.executeSql(
      "create table if not exists points (id integer primary key not null, value text, json text);",
      [],
      () => {
        console.log("table create success or already exist: points");
      },
      catchError
    );
  });
  db.transaction((tx) => {
    tx.executeSql(
      "create table if not exists participants (id integer primary key not null, name text unique not null, json text);",
      [],
      () => {
        console.log("table create success or already exist: participants");
      },
      catchError
    );
  });
  // addTestItem(db)
};

export const catchError = (arg1, arg2) => {
  console.log('ERROR', `argsL: ${arg1} ${arg2} file: database.js`,  null);
};

const addTestItem = (db) => {
  db.transaction((tx) => {
    tx.executeSql(
      "insert into items (value, json) values (?, ?);",
      [
        "test",
        JSON.stringify({
          rotateX: 70,
          rotateY: 0,
          rotateZ: 0,
          translateX: 0,
          translateY: 0,
          translateZ: 50, //translate Z is done to parent container
          perspective: 10,
          scale: 10,
          width: Dimensions.get("window").width / 2,
          height: Dimensions.get("window").height / 2,
          XGrids: 12,
          YGrids: 20,
          XSubGrids: 5,
          YSubGrids: 5,
          actualLength: null,
          actualWidth: null,
          pointX: 200,
          pointY: 200,
        }),
      ],
      () => {
        console.log("added test item");
      },
      catchError
    );
  });
};
