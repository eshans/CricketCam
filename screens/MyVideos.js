import React from "react";
import {
  Button,
  Text,
  // Header,
  Body,
  Icon,
  Title,
  Left,
  Right,
  Card,
  CardItem,
  Toast,
} from "native-base";
import ListItem from "react-native-elements/dist/list/ListItem";
import RNEIcon from "react-native-elements/dist/icons/Icon";
import RNEButton from "react-native-elements/dist/buttons/Button";
import Header from "react-native-elements/dist/header/Header";
import { Center } from "react-native-elements";
import {
  StyleSheet,
  View,
  Alert,
  Dimensions,
  ActivityIndicator,
  FlatList,
  SectionList,
} from "react-native";
import { Video } from "expo-av";
import VideoPlayer from "expo-video-player";
import Layout from "./Layout";
import shortid from "shortid";
import * as FileSystem from "expo-file-system";
import RotateView from "./rotateView2";
import {
  getItems,
  deleteItem,
  editItemJson,
} from "../components/database/items";
import {
  DEVELOPER_MODE,
  PRIMARY_COLOR,
  SCROLL_ITEM_HEIGHT,
  STYLES,
  TOAST_PROPS,
  VIDEO_CHANGE_RATE_INTERVAL,
  VIDEO_LOCATION,
  VIDEO_SEEK_INTERVAL,
} from "../constants/values";
import Player from "../components/VideoPlayer";
import { addPoint } from "../components/database/points";
import { Summary } from "../components/Summary";

import _ from "lodash";
import { Divider } from "react-native-elements/dist/divider/Divider";
import HelpButton from "../components/HelpDialog";
import ExportVideoToMediaLibraryButton from "../components/MediaLibrary";
import { RunTutorialAdditions } from "../components/tutorial";
import { decimalToFraction, deleteParticipantAlert } from "../constants/utils";
import Slider from "@react-native-community/slider";
import { EmptyComponent } from "./miscComponents/ListEmpty";
import { FadeInView, SlideDownView, SlideUpView } from "./miscComponents/Fade";
import { SYSTEM_BRIGHTNESS } from "expo-permissions";
import { deleteParticipant } from "../components/database/participants";

class RedirectTo extends React.Component {
  componentDidMount() {
    const { scene, navigation } = this.props;
    navigation.navigate(scene);
  }

  render() {
    return <RedirectTo scene={"/"} />;
  }
}

export default class MyVideos extends React.Component {
  constructor(props) {
    super(props);
    // this.state = {isToggleOn: true};

    // This binding is necessary to make `this` work in the callback
    this.setState = this.setState.bind(this);
    this.onPlaybackStatusUpdate = this.onPlaybackStatusUpdate.bind(this);
    this._video = React.createRef();
  }

  static navigationOptions = {
    header: () => (
      <Header>
        <Body>
          <Title>My videos 2</Title>
        </Body>
      </Header>
    ),
  };

  state = {
    videos: null,
    play: null,
    redirect: false,
    isPlaying: true, //remove
    isPlayingOver: false, //remove
    gridParams: null,
    playBackStatus: {}, //video playback status
    forwardbackwardRate: 100,
    scrollPosition: null,
    refreshingList: false,
    canVideoSeek: true,
    canSetRate: true,
    isSeeking: false,
    duration: null
  };

  componentDidUpdate() {
    console.log("component update");
    console.log(this.state.scrollPosition);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  getFileDetails = async (fileUri) => {
    let fileInfo = await FileSystem.getInfoAsync(fileUri);
    console.log("fileinfo", fileInfo);
    return true;
  };

  loadVideosList = async () => {
    console.log("load videos list");
    const dirInfo = await FileSystem.getInfoAsync(
      FileSystem.documentDirectory + "videos"
    );
    if (!dirInfo.exists) {
      console.log("videos directory doesnt exist");
      this.setState({ videos: [] });
      Toast.show({
        text: "Videos directory doesnt exist. Creating now...",
        ...TOAST_PROPS("info"),
      });
      await FileSystem.makeDirectoryAsync(
        `${FileSystem.documentDirectory}videos/`,
        {
          intermediates: true,
        }
      ).then(() => {
        this.loadVideosList();
      });
    }

    const videos = await FileSystem.readDirectoryAsync(
      FileSystem.documentDirectory + "videos"
    );

    // const videos = [];
    const videoData = getItems((items) => {
      this.setState({
        videoData: items,
      });
      // console.log(
      //   items.map((item) => ({ ...item, json: JSON.parse(item.json) }))
      // );
    });
    this.setState({ videos: videos.reverse() });
  };

  reloadVideosFromDatabase = () => {
    this.setState({ videoData: [] });

    getItems((items) => {
      this.setState({
        videoData: items,
      });
    });
  };

  gotoVideoListScreen = () => {
    console.log("go to videos list screen");
    this.reloadVideosFromDatabase();
    this.setState({ play: null, isEditing: false, positionMillisShow: null });
  };

  deleteVideo = (video) => {
    try {
      FileSystem.deleteAsync(`${VIDEO_LOCATION}/${video}`);
      Toast.show({
        text: "Video deleted!",
        ...TOAST_PROPS("info"),
      });
      deleteItem(video);
      this.loadVideosList();
    } catch (e) {
      Toast.show({
        text: "Video deleted unsuccessful",
        ...TOAST_PROPS("error"),
      });
      console.log("ERROR: ", e);
    }
  };

  async componentDidMount() {
    const { scene, navigation } = this.props;
    this.loadVideosList();
  }

  onPlaybackStatusUpdate(status) {
    console.log("state update");
    const playBackStatus = status;
    const position = playBackStatus?.positionMillis;
    const duration = playBackStatus?.durationMillis;
    const { didJustFinish } = playBackStatus;

    // console.log({ playBackStatus });
    if(this.state.duration != duration) {
      this.setState({duration : duration})

    }

    this.setState({
      isPlayingOver: position && position === duration,
      videoPosition: position,
      positionMillisShow: position,
      playBackStatus,
    });
  }

  deleteVideoAlert = (video) =>
    Alert.alert(
      `Delete ${video}`,
      "This action is not reversible",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        { text: "Delete", onPress: () => this.deleteVideo(video) },
      ],
      { cancelable: true }
    );

    

  // _mountVideo = (component) => {
  //   this._video = component;
  // };

  playVideo = async (video) => {
    this.setState({ play: video });
  };

  togglePlay = () => {
    if (this.state.isPlayingOver) {
      this._video.current.setStatusAsync({
        shouldPlay: false,
        positionMillis: 0,
      });
      this._video.current.setStatusAsync({ shouldPlay: true });
      this.state.isPlaying = false;
      this.setState({ isPlayingOver: false });
    }
    if (this.state.isPlaying) {
      this._video.current.pauseAsync();
    } else {
      this._video.current.playAsync();
    }
    this.setState({ isPlaying: !this.state.isPlaying });
  };

  videoForwardBackward = async (isForward) => {
    if (this.state.canVideoSeek) {
      this._video.current.getStatusAsync().then((state) => {
        const currentPos = Math.max(this.state.playBackStatus.positionMillis, 0);
        const nextPos = isForward
          ? currentPos + this.state.forwardbackwardRate
          : currentPos - this.state.forwardbackwardRate;
        console.log("forward ", nextPos);
        this.setState({ videoPosition: nextPos });
        this._video.current.setPositionAsync(nextPos, {
          toleranceMillisBefore: 100,
          toleranceMillisAfter: 100,
        });
      });
      this.setState({ canVideoSeek: false });
      setTimeout(
        () => this.setState({ canVideoSeek: true }),
        VIDEO_SEEK_INTERVAL
      );
    }
  };

  changeVideoRate = () => {
    const currentRate = this.state.playBackStatus.rate;
    const minRate = 1 / 8;
    const nextRate = currentRate == minRate ? 1 : currentRate / 2;

    if (this.state.canSetRate) {
      this._video.current.setRateAsync(nextRate, true, 1);
      this.setState({ canSetRate: false });
      setTimeout(
        () => this.setState({ canSetRate: true }),
        VIDEO_CHANGE_RATE_INTERVAL
      );
    }
  };

  setGridParams = (params) => {
    this.setState({ gridParams: params });
  };

  handleScroll = (event) => {
    this.setState({
      scrollPosition: event.nativeEvent.contentOffset.y / SCROLL_ITEM_HEIGHT,
    });
  };

  refreshList = () => {
    this.setState({ refreshingList: true });
    // Toast.show({
    //   text: "Reloading...",
    //   ...TOAST_PROPS("info"),
    // });
    this.loadVideosList();
    this.reloadVideosFromDatabase();
    this.setState({ refreshingList: false });
  };

  handleVideoSeek = (val) => {
    if (this.state.canVideoSeek) {
      this._video.current.setPositionAsync(val, {
        toleranceMillisBefore: 50,
        toleranceMillisAfter: 50,
      });
      this.setState({ canVideoSeek: false });
      setTimeout(
        () => this.setState({ canVideoSeek: true }),
        VIDEO_SEEK_INTERVAL
      );
    }
  };

  handleVideoSeekComplete = (val) => {
    setTimeout(() => {
      this._video.current.setPositionAsync(val, {
        toleranceMillisBefore: 50,
        toleranceMillisAfter: 50,
      });
      this.setState({ positionMillisShow: val, isSeeking: false });
    }, VIDEO_SEEK_INTERVAL);
  };

  render() {
    const {
      videos,
      play,
      redirect,
      isPlaying,
      videoData,
      viewSummary,
      playBackStatus,
    } = this.state;
    const data = _.chain(videoData)
      // Group the elements of Array based on `color` property
      .groupBy(function (b) {
        return b?.json?.participant;
      })
      // `key` is group's name (color), `value` is the array of objects
      .map((value, key) => ({ title: key, data: _.orderBy(value, "json") }))
      .value();

    if (redirect) {
      return <RedirectTo scene={redirect} navigation={this.props.navigation} />;
    }
    if (viewSummary) {
      const videoRecords = videoData.filter(
        (d) => d.json.participant === viewSummary
      );
      const videoSavedState = videoRecords[0]?.json;
      return (
        <Summary
          plays={videoRecords}
          savedState={videoSavedState}
          myVideosSetState={this.setState}
        />
      );
    } else if (play) {
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      //////////////////////////////////////////////////     Video Player

      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      console.log({ play });
      return (
        <FadeInView style={styles.containerCenter} duration={500}>
          <HelpButton screen={"videoPlayer"} />
          <Player
            _video={this._video}
            play={play}
            onPlaybackStatusUpdate={this.onPlaybackStatusUpdate}
            // setState ={this.setState}
          />
          <RotateView
            isEditing={this.state.isEditing}
            savedState={
              this.state.gridParams
                ? this.state.gridParams
                : videoData &&
                  videoData.find((d) => `${d.value}` === play)?.json
            }
            canAddPoints={!this.state.isPlaying && !this.state.isEditing}
            play={play}
            setGridParams={this.setGridParams}
            screen={"addPoint"}
          />
          
          <View
            style={[
              {
                position: "absolute",
                bottom: 60,
                backgroundColor: "transparent",
                zIndex: 1000000,
              },
            ]}
          >
            <Slider
              tapToSeek={true}
              style={{
                width: Dimensions.get("window").width,
                height: 40,
                zIndex: 1000000,
              }}
              minimumValue={0}
              maximumValue={this.state.duration}
              minimumTrackTintColor="#000000"
              maximumTrackTintColor="#000000"
              step={10}
              onSlidingComplete={this.handleVideoSeekComplete}
              onSlidingStart={() => {
                this._video.current.pauseAsync();
                this.setState({ isPlaying: false, isSeeking: true });
              }}
              onValueChange={this.handleVideoSeek}
              // onSlidingComplete={(val) =>
              //   this.setState({ posisionMillisShow: val })
              // }
              value={
                this.state.isSeeking ? this.state.positionMillisShow : playBackStatus.positionMillis
              }
            />
          </View>
          <SlideUpView style={STYLES.bottomActions}>
            <View
              style={{
                flex: 0.15,
                flexDirection: "row-reverse",
                width: 10, //Dimensions.get("window").width/32,
                justifyContent: "flex-end",
                alignSelf: "flex-start",
                // ...styles.bottomButton,
                // ...{ borderWidth: 1, borderColor: "black" },
              }}
            >
              <Button
                // disabled={recording}
                style={styles.bottomButton}
                transparent
                onPress={() =>
                  this.setState({ isEditing: !this.state.isEditing })
                }
              >
                <Icon name="ios-settings" />
              </Button>
              <Button
                // disabled={recording}
                style={styles.bottomButton}
                transparent
                onPress={this.gotoVideoListScreen}
              >
                <Icon name="ios-arrow-back-circle" />
              </Button>
            </View>
            <View
              style={{
                flex: 0.7,
                flexDirection: "row-reverse",
                width: Dimensions.get("window").width / 2,
                justifyContent: "center",
                alignSelf: "center",
                ...styles.bottomButton,
              }}
            >
              <Button
                transparent
                style={styles.bottomButton}
                disabled={
                  playBackStatus.isPlaying || playBackStatus.isBuffering
                }
                onPress={() => this.videoForwardBackward(true)}
              >
                <Icon name="caret-forward" />
              </Button>
              <Button
                style={styles.bottomButton}
                danger
                onPress={() => {
                  this.togglePlay();
                }}
              >
                {playBackStatus.positionMillis ===
                playBackStatus.durationMillis ? (
                  <Icon name="ios-refresh" />
                ) : playBackStatus.isPlaying ? (
                  <Icon name="ios-pause" />
                ) : (
                  <Icon name="ios-play" />
                )}
              </Button>

              <Button
                transparent
                style={styles.bottomButton}
                disabled={
                  playBackStatus.isPlaying || playBackStatus.isBuffering
                }
                onPress={() => this.videoForwardBackward(false)}
              >
                <Icon name="caret-back" />
              </Button>
            </View>
            <View
              style={{
                flex: 0.15,
                flexDirection: "row-reverse",
                width: 10, //Dimensions.get("window").width/32,
                justifyContent: "flex-start",
                alignSelf: "flex-start",
                // ...styles.bottomButton,
                // ...{ borderWidth: 1, borderColor: "black" },
              }}
            >
              <Button
                // disabled={recording}
                style={[styles.bottomButton, { borderRadius: 1000 }]}
                onPress={this.changeVideoRate}
              >
                <Text style={{ fontSize: 12 }}>{`X${decimalToFraction(
                  playBackStatus.rate
                )}`}</Text>
              </Button>
              <View
                styles={{
                  flexDirection: "column",
                  marginRight: 100,
                  margin: 5,
                }}
              >
                <Text
                  style={{ alignSelf: "center", color: "grey", marginTop: 5 }}
                >
                  {`Step rate: ${
                    this.state.forwardbackwardRateShow ||
                    this.state.forwardbackwardRate
                  }`}
                </Text>

                <Slider
                  style={{ width: 200, height: 40 }}
                  minimumValue={10}
                  maximumValue={200}
                  minimumTrackTintColor="#FFFFFF"
                  maximumTrackTintColor="#000000"
                  step={10}
                  onSlidingComplete={(val) =>
                    this.setState({ forwardbackwardRate: val })
                  }
                  onValueChange={(val) =>
                    this.setState({ forwardbackwardRateShow: val })
                  }
                  value={this.state.forwardbackwardRate}
                />
              </View>

              {/* <Button
                // disabled={recording}
                style={styles.bottomButton}
                transparent
              >
                <Icon name="information" />
              </Button> */}
            </View>
          </SlideUpView>
        </FadeInView>
      );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////     Videos list

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    else {
      // if (this._SectionList && this.state.scrollPosition) {
      //   this.autoScrollToLastPos();
      // }

      return (
        <FadeInView style={styles.viewContainer}>
          <HelpButton screen={"videosList"} />
          <SlideDownView>
            <Header
              style={{ height: 50 }}
              placement="left"
              centerComponent={{ text: "VIDEOS", style: { color: "#fff" } }}
            />
          </SlideDownView>
          {/* <Text>test</Text> */}
          {/* <Text>{JSON.stringify(data)}</Text> */}

          <Card style={[styles.fillSpace]}>
            {!videos && <ActivityIndicator style={styles.fillSpace} />}
            {!videos && <RunTutorialAdditions />}
            {videos && (
              <SectionList
                // decelerationRate={0}
                getItemLayout={(data, index) => ({
                  length: SCROLL_ITEM_HEIGHT,
                  offset: SCROLL_ITEM_HEIGHT * index,
                  index,
                })}
                initialScrollIndex={
                  data ? Math.round(this.state.scrollPosition) : -1
                }
                // contentOffset={{x: 0, y: this.state.scrollPosition || 0}}
                ref={this._SectionList}
                onMomentumScrollEnd={this.handleScroll}
                onScrollEndDrag={this.handleScroll}
                sections={data}
                keyExtractor={(item, index) => item.id + index}
                refreshing={this.state.refreshingList}
                onRefresh={this.refreshList}
                scrollEventThrottle={2000}
                // removeClippedSubviews={true}
                ListEmptyComponent={
                  <EmptyComponent
                    icon="videocam-outline"
                    mainText={"No Videos!"}
                    secondaryText={"Record a video"}
                    tertiaryText={"by going into the camera view"}
                  />
                }
                renderSectionHeader={({ section: { title } }) => (
                  <FadeInView
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      backgroundColor: "#ccc",
                      ...STYLES.baseHeader,
                      height: SCROLL_ITEM_HEIGHT,
                    }}
                  >
                    <Divider />
                    <RNEButton
                      title="Summary"
                      icon={{ name: "info", color: "white" }}
                      buttonStyle={{
                        ...STYLES.baseButton,
                        minHeight: "50%",
                        margin: 10,
                      }}
                      onPress={() => this.setState({ viewSummary: title })}
                    />
                    {/* <Icon
                      name="information-circle-outline"
                      onPress={() => this.setState({ viewSummary: title })}
                      style={{padding: 5}}
                    /> */}
                    <ListItem.Content>
                      <Text style={{ padding: 10, fontWeight: "bold" }}>
                        {title}
                      </Text>
                    </ListItem.Content>
                    <RNEButton
                        // title="Delete"
                        icon={{ name: "delete", color: "white" }}
                        buttonStyle={{
                          backgroundColor: "red",
                          margin: 10
                        }}
                        onPress={() => deleteParticipantAlert(`${title}`)}
                      />
                  </FadeInView>
                )}
                renderItem={({ item, index }) => (
                  <FadeInView>
                    <ListItem
                      key={item}
                      style={{ height: SCROLL_ITEM_HEIGHT }}
                      // leftContent={
                      // <RNEButton
                      //   title="Info"
                      //   icon={{ name: "info", color: "white" }}
                      //   buttonStyle={{ minHeight: "100%" }}
                      // />
                      // }
                      // rightContent={
                      //   <RNEButton
                      //     title="Delete"
                      //     icon={{ name: "delete", color: "white" }}
                      //     buttonStyle={{
                      //       minHeight: "100%",
                      //       backgroundColor: "red",
                      //     }}
                      //     onPress={() =>
                      //       this.deleteVideoAlert(`${item.value}`)
                      //     }
                      //   />
                      // }
                      onPress={() => this.playVideo(`${item.value}`)}
                    >
                      <Icon name="videocam-outline" />
                      <ListItem.Content>
                        <ListItem.Title>{`${index}. ${item.value}`}</ListItem.Title>
                      </ListItem.Content>
                      {/* <RNEButton
                      
                      icon={{ name: "info", color: "blue" }}
                      buttonStyle={{
                        minHeight: "100%",
                        backgroundColor: "white",
                      }}
                      onPress={() => this.setState({ viewSummary: item })}
                    /> */}
                      <RNEButton
                        // title="Delete"
                        icon={{ name: "delete", color: "white" }}
                        buttonStyle={{
                          backgroundColor: "red",
                        }}
                        onPress={() => this.deleteVideoAlert(`${item.value}`)}
                      />
                      <ExportVideoToMediaLibraryButton
                        localUri={`${FileSystem.documentDirectory}videos/${item.value}`}
                      />
                      {/* <Icon
                      name="information-circle-outline"
                      onPress={() => this.setState({ viewSummary: item.value })}
                    /> */}
                      <ListItem.Chevron />
                    </ListItem>
                  </FadeInView>

                  // <CardItem key={video}>
                  //   <Left>
                  //     <Text>{video}</Text>
                  //   </Left>
                  //   <Right>
                  //     <Button danger onPress={() => this.playVideo(video)}>
                  //       <Icon ios="ios-play" android="md-play" />
                  //     </Button>
                  //   </Right>
                  // </CardItem>
                )}
              />
            )}
            <View style={{ height: 60 }} />
          </Card>
          <SlideUpView
            style={{ ...styles.bottomActions, justifyContent: "center" }}
          >
            <Button
              transparent
              iconRight
              onPress={() => this.props.navigation.navigate("Camera")}
            >
              {/* <Text>Back to camera</Text>
              <Icon name='ios-arrow-back'/> */}
              <Icon name="camera" />
            </Button>
          </SlideUpView>
        </FadeInView>
      );
    }
  }
}

const styles = StyleSheet.create({
  actionButtons: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
  },
  container: {
    flexDirection: "column",
    justifyContent: "space-between",
  },
  videoContainer: {
    flexDirection: "column",
    flexGrow: 1,
  },
  bottomActions: STYLES.bottomActions,
  containerCenter: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    // width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
  containerCamera: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
  bottomButton: {
    zIndex: 1000,
    // width: 100,
    margin: 10,
  },
  viewContainer: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    backgroundColor: "white",
  },
  fillSpace: {
    flex: 1,
    alignSelf: "stretch",
    backgroundColor: "white",
    margin: 5,
  },
});
