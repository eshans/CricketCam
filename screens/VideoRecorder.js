import React from "react";
import {
  Button,
  Text,
  Header,
  Body,
  Icon,
  Title,
  Spinner,
  Toast,
} from "native-base";
import * as FileSystem from "expo-file-system";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
} from "react-native";
import Layout from "./Layout";
import delay from "delay";
import shortid from "shortid";
import * as Permissions from "expo-permissions";
import { Camera } from "expo-camera";
import { Audio } from "expo-av";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import { RecordScreen } from "react-native-record-screen";
import RotateView from "./rotateView2";
import { ButtonGroup } from "react-native-elements";
import { addItem } from "../components/database/items";
import {
  DEVELOPER_MODE,
  INITIAL_GRID_PARAMS,
  STYLES,
  TOAST_PROPS,
} from "../constants/values";
import AddParticipantButton from "../components/AddParticipantDialog";
import HelpButton, { HelpDialog } from "../components/HelpDialog";
import { SlideUpView } from "./miscComponents/Fade";

class RedirectTo extends React.Component {
  componentDidMount() {
    const { scene, navigation, clearState } = this.props;
    const tempScene = scene;
    clearState();
    navigation.navigate(tempScene);
  }

  render() {
    const { scene, navigation, clearState } = this.props;
    return (
      <View>
        <Text>{JSON.stringify(scene)}</Text>
      </View>
    );
  }
}

const printChronometer = (seconds) => {
  const minutes = Math.floor(seconds / 60);
  const remseconds = seconds % 60;
  return (
    "" +
    (minutes < 10 ? "0" : "") +
    minutes +
    ":" +
    (remseconds < 10 ? "0" : "") +
    remseconds
  );
};

export default class VideoRecorder extends React.Component {
  static navigationOptions = {
    header: () => (
      <Header>
        <Body>
          <Title>My videos 3</Title>
        </Body>
      </Header>
    ),
  };

  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    recording: false,
    duration: 0,
    redirect: false,
    isEditing: false,
    loading: true,
    gridParams: INITIAL_GRID_PARAMS,
  };

  async componentDidMount() {
    this.requestPermissions()
  }

  requestPermissions = async () => {
    const { status: cameraStatus } = await Camera.requestPermissionsAsync();
    const { status: audioStatus } = await Audio.requestPermissionsAsync();
    
    this.setState({
      hasCameraPermission:
        cameraStatus === "granted" && audioStatus === "granted",
    });
    this.setState({ ...this.state, loading: false });
  }

  async registerRecord() {
    const { recording, duration } = this.state;

    if (recording) {
      await delay(1000);
      this.setState((state) => ({
        ...state,
        duration: state.duration + 1,
      }));
      this.registerRecord();
    }
  }

  async startRecording() {
    if (!this.camera) {
      return;
    }

    Toast.show({
      text: "Recording started!",
      ...TOAST_PROPS("info"),
    });

    await this.setState((state) => ({ ...state, recording: true }));
    this.registerRecord();
    const record = await this.camera.recordAsync();
    // console.log("uri: ", record.uri);
    const d = new Date();
    const videoId = `${d.toISOString().slice(0, 10)}_${d
      .toString()
      .slice(16, 24)}`;
    console.log({ videoId });

    await FileSystem.makeDirectoryAsync(
      `${FileSystem.documentDirectory}videos/`,
      {
        intermediates: true,
      }
    );

    await FileSystem.moveAsync({
      from: record.uri,
      to: `${FileSystem.documentDirectory}videos/${videoId}.mov`,
    });

    addItem(`${videoId}.mov`, this.state.gridParams); //add to database

    // console.log(`${FileSystem.documentDirectory}videos/demo_${videoId}.mov`);
    // this.setState((state) => ({ ...state, redirect: "MyVideos" }));
  }

  async stopRecording() {
    if (!this.camera) {
      return;
    }

    await this.camera.stopRecording();

    Toast.show({
      text: "Recording successful!",
      ...TOAST_PROPS("success"),
    });
    this.setState((state) => ({ ...state, recording: false, duration: 0 }));
  }

  toggleRecording() {
    const { recording } = this.state;

    return recording ? this.stopRecording() : this.startRecording();
  }

  clearState = () => {
    this.setState({});
  };

  setGridParams = (params) => {
    this.state.gridParams = params;
  };

  toggleIsEditingTrue = () => {
    this.setState({isEditing: !this.state.isEditing})
    console.log('toggle isEditing')

  }

  render() {
    const { hasCameraPermission, recording, duration, redirect } = this.state;

    if (redirect) {
      return (
        <RedirectTo
          scene={redirect}
          navigation={this.props.navigation}
          clearState={this.clearState}
        />
      );
    }

    if (hasCameraPermission === null) {
      this.requestPermissions()
      return (
        <Layout style={styles.containerCenter}>
          <Spinner />
        </Layout>
      );
    } else if (hasCameraPermission === false) {
      return (
        <Layout style={styles.containerCenter}>
          <Text>No access to camera</Text>;
        </Layout>
      );
    } else {
      return (
        <View style={styles.containerCenter}>
          <HelpButton
            screen={
              this.state.isEditing ? "RecordGridControls" : "videoRecorder"
            }
          />
          <Camera
            style={styles.containerCamera}
            type={this.state.type}
            ref={(ref) => {
              this.camera = ref;
            }}
          ></Camera>
          <RotateView
            isEditing={this.state.isEditing}
            setGridParams={this.setGridParams}
            canAddPoints={false}
          />

          {/* <Grid /> */}
          {/* <RecordScreenZone> */}
          
          {/* </RecordScreenZone> */}
          <SlideUpView style={STYLES.bottomActions}>
            <Button
              disabled={recording}
              style={styles.bottomButton}
              transparent
              onPress={this.toggleIsEditingTrue
                
              }
            >
              <Icon name="ios-settings" />
            </Button>
            {/* <AddParticipantButton /> */}
            <View
              style={{
                flex: 1,
                flexDirection: "row-reverse",
                width: Dimensions.get("window").width / 2,
                justifyContent: "center",
                alignSelf: "center",
                ...styles.bottomButton,
              }}
            >
              <Button
                danger
                disabled={this.state.isEditing}
                onPress={() => {
                  this.toggleRecording();
                }}
              >
                {recording ? (
                  <Icon ios="ios-square" android="md-square" />
                ) : (
                  <Icon
                    ios="ios-radio-button-on"
                    android="md-radio-button-on"
                  />
                )}
              </Button>
            </View>
            {DEVELOPER_MODE && (
                  <Button
                    icon
                    transparent
                    style={styles.bottomButton}
                    onPress={() => this.props.navigation.navigate("DBVIZ")}
                  >
                    <Icon ios="ios-settings" android="md-folder" />
                  </Button>
                )}
            {recording ? (
              <Button transparent style={{ ...styles.bottomButton }}>
                <Icon ios="ios-recording" android="md-recording" />
                <Text secondary>{printChronometer(duration)}</Text>
              </Button>
            ) : (
                <Button
                  icon
                  transparent
                  style={styles.bottomButton}
                  onPress={() => this.props.navigation.navigate("MyVideos")}
                >
                  <Icon ios="ios-folder" android="md-folder" />
                </Button>
            )}
          </SlideUpView>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  topActions: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "transparent",
  },
  bottomButton: {
    zIndex: 1000,
    margin: 10,
    width: 100,
    alignContent: "flex-end",
  },
  bottomActions: STYLES.bottomActions,
  containerCenter: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
  },
  containerCamera: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
  container: {
    position: "absolute",
    // left: Dimensions.get('width') / 2,
    // // y: Dimensions.get('width') / 2,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    zIndex: 100,
  },
});
