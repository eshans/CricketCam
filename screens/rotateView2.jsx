import React, { Component, useRef } from "react";
import {
  PanResponder,
  View,
  Image,
  Dimensions,
  Text,
  ScrollView,
  TextInput,
  SafeAreaView,
  Select,
  Alert,
} from "react-native";
import { Button, Icon, Picker, Toast } from "native-base";
import { Svg, Line, Circle, Polygon, Text as SvgText } from "react-native-svg";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { Tooltip } from "react-native-elements";
import {
  BORDER_STATE,
  DEVELOPER_MODE,
  DISABLED_COLOR,
  INITIAL_GRID_PARAMS as initialState,
  INITIAL_POINT_STATE,
  IPHONE_MODE,
  LENGTH_UNIT,
  PITCH_COLOR,
  PRIMARY_COLOR,
  REAL_PITCH_WIDTH,
  SELECT_MARKER_COLORS,
  STUMPS_GAP,
  STYLES,
  TOAST_PROPS,
} from "../constants/values";
import {
  capitalizeFirstLetter,
  drawPitchBorder,
  generateDistanceAreas,
  generateDistanceMarkersLines,
  generateLines,
  generatePitchHorizontalLines,
  generatePitchPoints,
  generatePitchVerticalLines,
  generatePointsString,
  generateWicketArea,
  generateWicketPoints,
  getValWithScale,
  getWicketY,
  log,
  ObjectDifference,
  to2DecimalPlaces,
} from "../constants/utils";
import { Value } from "react-native-reanimated";

import { GridControls } from "../components/GridControls";
import { addPoint } from "../components/database/points";
import { getParticipants } from "../components/database/participants";
import DataGrid from "../components/dataGrid";
// import { Picker } from "native-base";
import _ from "lodash";
import { AddPoints } from "../components/AddPoints";
import { editItemJson } from "../components/database/items";
import { FadeInView, SlideDownView } from "./miscComponents/Fade";
import { throwIfAudioIsDisabled } from "expo-av/build/Audio/AudioAvailability";

export default class RotateView extends Component {
  state = this.props.savedState
    ? {
        ...this.props.savedState,
        pointColor: initialState.pointColor,
        ...INITIAL_POINT_STATE,
      }
    : { ...BORDER_STATE, selectedPoint: "UL" };

  

  handlePanResponderMove(e, gestureState) {
    // const { dx, dy } = gestureState;
    // const y = `${dx}deg`;
    // const x = `${-dy}deg`;
    // this.setState({
    //   ...this.state,
    //   pointX: Math.min(
    //     Math.max(this.state.pointX + dx / 10, 0),
    //     getValWithScale(this.state.width, this.state.scale)
    //   ),
    //   pointY: Math.min(
    //     Math.max(this.state.pointY + dy / 10, 0),
    //     getValWithScale(this.state.height, this.state.scale)
    //   ),
    // });
    // console.log({ "pan res": dx, dy });
  }

  shouldComponentUpdate(props) {
    return true;
  }

  componentDidMount() {
    this.setState(this.state);
  }

  componentDidUpdate() {}

  onSaveStateToExistingVideo = () => {
    //TODO save only required state keys and values
    editItemJson(this.props.play, this.state);
    log("INFO", "onSaveStateToExistingVideo", "file: rotateView.js");
  };

  render() {
    const getCornersFromPoints = (points) => {
      // console.log({pointsx: this.state.borderPoints})
      return [
        [points.UL, points.UR],
        [points.DL, points.DR],
      ];
    };

    const pitchPoints = generatePitchPoints(this.state.borderPoints);

    return (
      <>
      <View
          key={"addpoints-view5"}
          style={{
            position: "absolute",
            width: Dimensions.get("window").width,
            top: 0,
            flex: 1,
            justifyContent: "top",
            alignItems: "center",
            backgroundColor: "white",
            zIndex: 3000,
          }}
        >
          <AddPoints
            key={"addpoints-main"}
            pointX={this.state.pointX}
            pointY={this.state.pointY}
            participant={this.state.participant}
            play={this.props.play}
            show={this.props.canAddPoints || this.state.canAddPoints}
            scale={this.state.scale}
            borderPoints={this.state.borderPoints}
            actualLength={this.state.actualLength}
            actualWidth={this.state.actualWidth}
          />
          {DEVELOPER_MODE && this.state.canAddPoints && (
            <Button onPress={() => this.setState({ canAddPoints: false })}>
              <Text>hide addpoint</Text>
            </Button>
          )}
        </View>
        <View
          onTouchMove={
            this.props.isEditing
              ? (e) => {
                  const newBorderPoints = this.state.borderPoints;
                  if (this.state.selectedPoint == "MS") {
                    newBorderPoints[this.state.selectedPoint] = {
                      x: e.nativeEvent.locationX,
                      y: getWicketY(
                        this.state.borderPoints,
                        e.nativeEvent.locationX
                      ),
                    };

                    newBorderPoints["MS"] = {
                      x:
                        (this.state.borderPoints.UL.x +
                          this.state.borderPoints.UR.x) /
                        2,
                      y: getWicketY(
                        this.state.borderPoints,
                        (this.state.borderPoints.UL.x +
                          this.state.borderPoints.UR.x) /
                          2
                      ),
                    };
                  } else if (
                    String(this.state.selectedPoint).search("R") != -1
                  ) {
                    const point =
                      String(this.state.selectedPoint).search("U") != -1
                        ? "UL"
                        : "DL";
                    newBorderPoints[this.state.selectedPoint] = {
                      y: newBorderPoints[point].y,
                      x: e.nativeEvent.locationX,
                    };
                  } else {
                    newBorderPoints[this.state.selectedPoint] = {
                      x: e.nativeEvent.locationX,
                      y: e.nativeEvent.locationY,
                    };

                    // const otherPoint = String(this.state.selectedPoint).search('U') != -1 ? 'UR': 'DR'
                    // console.log({
                    //   y: this.state.borderPoints[otherPoint].y,
                    //   x: e.nativeEvent.locationX,
                    // })
                    // newBorderPoints[otherPoint] = {
                    //   y: this.state.borderPoints[otherPoint].y,
                    //   x: e.nativeEvent.locationX,
                    // };
                  }

                  newBorderPoints["MS"] = {
                    x:
                      (this.state.borderPoints.UL.x +
                        this.state.borderPoints.UR.x) /
                      2,
                    y: getWicketY(
                      this.state.borderPoints,
                      (this.state.borderPoints.UL.x +
                        this.state.borderPoints.UR.x) /
                        2
                    ),
                  };

                  this.setState({
                    ...this.state,

                    borderPoints: newBorderPoints,
                  });
                  // console.log('border points ',this.state.borderPoints);
                }
              : this.props.canAddPoints
              ? (e) => {
                  const newState = this.state;
                  newState["pointX"] = e.nativeEvent.locationX;
                  newState["pointY"] = e.nativeEvent.locationY;
                  this.setState(newState);
                }
              : null
          }
          key={"panresponderview"}
          ref={(component) => (this.refContainer = component)}
          style={styles.container}
          //   {...this.panResponder.panHandlers}
        >
          <View
            key={"panresponderview2"}
            ref={(component) => (this.refView = component)}
            style={{ ...styles.rotateView }}
          >
            <Svg>
              {true &&
                generateLines(
                  getCornersFromPoints(this.state.borderPoints)
                ).map((point, index) => (
                  <Line
                    key={index}
                    x1={point[0].x}
                    y1={point[0].y}
                    x2={point[1].x}
                    y2={point[1].y}
                    stroke="white"
                    strokeWidth={`4`}
                  />
                ))}
              {/* {true &&
                generateLines(
                  getCornersFromPoints(pitchPoints)
                ).map((point, index) => (
                  <Line
                    key={index}
                    x1={point[0].x}
                    y1={point[0].y}
                    x2={point[1].x}
                    y2={point[1].y}
                    stroke="white"
                    strokeWidth={`4`}
                  />
                ))} */}
              <Polygon
                key={"test"}
                points={generatePointsString(this.state.borderPoints)}
                fill={PITCH_COLOR}
                stroke="purple"
                strokeWidth="0"
              />
              {this.props.canAddPoints | this.state.canAddPoints | true ? (
                <Circle
                  key="addpoint-point"
                  cx={this.state.pointX}
                  cy={this.state.pointY}
                  r={`10`}
                  stroke={"yellow"}
                  strokeWidth="2"
                  fill={"white"}
                />
              ) : null}

              {Object.keys(this.state.borderPoints).map((key) => {
                return (
                  <Circle
                    key={key}
                    cx={this.state.borderPoints[key].x}
                    cy={this.state.borderPoints[key].y}
                    r={"10"}
                    stroke={"red"}
                    strokeWidth="2"
                    fill={
                      this.state.selectedPoint == key && this.props.isEditing
                        ? "red"
                        : key == "MS"
                        ? "blue"
                        : "green"
                    }
                    onPressIn={
                      this.props.isEditing
                        ? () => this.setState({ selectedPoint: key })
                        : () => {}
                    }
                  />
                );
              })}
            </Svg>
          </View>
        </View>
        {this.props.isEditing && (
          <SlideDownView
            key={"main_view"}
            style={{
              position: "absolute",
              width: Dimensions.get("window").width,
              top: 0,
              flex: 1,
              justifyContent: "top",
              alignItems: "center",
              backgroundColor: "white",
              zIndex: 3000,
              borderRadius: 10,
            }}
          >
            {DEVELOPER_MODE && (
              <Button onPress={() => this.setState({ canAddPoints: true })}>
                <Text>show addpoint</Text>
              </Button>
            )}

            <GridControls
              key={"grid_controls"}
              onChangeValue={this.onChangeValue}
              onReset={this.onReset}
              state={this.state}
              setState={(state) => this.setState(state)}
              onStateReset={this.onStateReset}
              screen={this.props.screen}
              onSaveStateToExistingVideo={
                this.props.screen == "addPoint"
                  ? this.onSaveStateToExistingVideo
                  : () => {
                      log("no existing video state");
                    }
              }
            />
          </SlideDownView>
        )}
      </>
    );
  }
}

const styles = {
  scrollView: {
    maxHeight: 400,
  },
  container: {
    position: "absolute",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    zIndex: 0,
    top: initialState.translateZ,
    // backgroundColor: "blue",
  },

  rotateView: {
    justifyContent: "center",
    alignItems: "center",
    zIndex: 0,
    // position: "absolute",
    // backgroundColor: "rgba(0, 52, 0, 0.5)",
    zIndex: 100,
    transform: [
      { perspective: initialState.perspective * 100 },
      { rotateX: `${initialState.rotateX}deg` },
    ],
    width: initialState.width,
    height: initialState.height,
  },
  // rotateView2: {
  //   justifyContent: "center",
  //   alignItems: "center",
  //   zIndex: 1000,
  //   position: "absolute",
  //   backgroundColor: "rgba(0, 52, 0, 0.5)",
  //   zIndex: 100,
  //   transform: [
  //     { perspective: 1000 },
  //     { rotateX: `${initialState.rotateX + 20}deg` },
  //   ],
  //   width: initialState.width * (parseFloat(initialState.scale) / 10),
  //   height: initialState.height * (parseFloat(initialState.scale) / 10),
  // },
  topActions: {
    marginRight: 100,
    marginLeft: 100,
    flexDirection: "row",
    // justifyContent: "space-between",
    backgroundColor: "transparent",
    zIndex: 1000,
  },
  controlContainer: {
    padding: 10,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonContainer: {
    flex: 0,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  numberInput: {
    margin: 5,
    borderColor: "#aaaaaa",
    borderWidth: 1,
    borderRadius: 5,
    padding: 7,
    width: 100,
  },

  splitRight: {
    right: 0,
    width: Dimensions.get("window").width / 2,
    position: "fixed",
    zIndex: 1,
    top: 0,
    // overflowX: "hidden",
    paddingTop: 20,
  },
  splitLeft: {
    left: 0,
    width: Dimensions.get("window").width / 2,
    position: "fixed",
    zIndex: 1,
    top: 0,
    // overflowX: "hidden",
    paddingTop: 20,
  },
  centered: {
    position: "absolute",
    alignItems: "center",
    // transform: translate("-50%", "-50%"),
    textAlign: "center",
  },
};
