import React, { useRef, useEffect } from 'react';
import { Animated, Text, View } from 'react-native';

export const FadeInView = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current  // Initial value for opacity: 0

  React.useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        delay: props.delay || 0,
        toValue: 1,
        duration: props.duration || Math.random() * 500,
        useNativeDriver: true
      }
    ).start();
    
  }, [fadeAnim])

  return (
    <Animated.View                 // Special animatable View
      style={{
        ...props.style,
        opacity: fadeAnim,  
      }}
    >
      {props.children}
    </Animated.View>
  );
}

// You can then use your `FadeInView` in place of a `View` in your components:


export const SlideDownView = (props) => {
    const fadeAnim = useRef(new Animated.Value(0)).current  // Initial value for opacity: 0
    const slideDownAnim = useRef(new Animated.Value(1)).current
  
    React.useEffect(() => {
      Animated.timing(
        fadeAnim,
        {
          toValue: 1,
          duration: 250,
          useNativeDriver: true
        }
      ).start();
      Animated.timing(
          slideDownAnim,
          {
            
            toValue: 0,
            duration: 250,
            useNativeDriver: true,
            
          }
        ).start();
  
      return () => Animated.timing(
        slideDownAnim,
        {
          toValue: 1,
          duration: 250,
          useNativeDriver: true,
        }
      ).start();
    }, [fadeAnim])
  
    return (
      <Animated.View                 // Special animatable View
        style={{
          ...props.style,
          opacity: fadeAnim,  
          transform: [{
              translateY: slideDownAnim.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, -1000]
          })}],
        }}
        
      >
        {props.children}
      </Animated.View>
    );
  }

  export const SlideUpView = (props) => {
    const fadeAnim = useRef(new Animated.Value(0)).current  // Initial value for opacity: 0
    const slideUpAnim = useRef(new Animated.Value(0)).current
  
    React.useEffect(() => {
      Animated.timing(
        fadeAnim,
        {
          toValue: 1,
          duration: 250,
          useNativeDriver: true
        }
      ).start();
      Animated.timing(
          slideUpAnim,
          {
            
            toValue: 1,
            duration: 350,
            useNativeDriver: true,
            
          }
        ).start();
  
    }, [fadeAnim])
  
    return (
      <Animated.View                 // Special animatable View
        style={{
          ...props.style,
          opacity: fadeAnim,  
          transform: [{
              translateY: slideUpAnim.interpolate({
                  inputRange: [0, 1],
                  outputRange: [100, 0]
          })}],
        }}
        
      >
        {props.children}
      </Animated.View>
    );
  }

  export const SlideFromRightView = (props) => {
    const fadeAnim = useRef(new Animated.Value(0)).current  // Initial value for opacity: 0
    const slideUpAnim = useRef(new Animated.Value(0)).current
  
    React.useEffect(() => {
      Animated.timing(
        fadeAnim,
        {
          toValue: 1,
          duration: 250,
          useNativeDriver: true
        }
      ).start();
      Animated.timing(
          slideUpAnim,
          {
            
            toValue: 1,
            duration: 350,
            useNativeDriver: true,
            
          }
        ).start();
  
    }, [fadeAnim])
  
    return (
      <Animated.View                 // Special animatable View
        style={{
          ...props.style,
          opacity: fadeAnim,  
          transform: [{
              translateX: slideUpAnim.interpolate({
                  inputRange: [0, 1],
                  outputRange: [100, 0]
          })}],
        }}
        
      >
        {props.children}
      </Animated.View>
    );
  }

  export const SlideFromLeftView = (props) => {
    const fadeAnim = useRef(new Animated.Value(0)).current  // Initial value for opacity: 0
    const slideUpAnim = useRef(new Animated.Value(0)).current
  
    React.useEffect(() => {
      Animated.timing(
        fadeAnim,
        {
          toValue: 1,
          duration: 250,
          useNativeDriver: true
        }
      ).start();
      Animated.timing(
          slideUpAnim,
          {
            
            toValue: 1,
            duration: 350,
            useNativeDriver: true,
            
          }
        ).start();
  
    }, [fadeAnim])
  
    return (
      <Animated.View                 // Special animatable View
        style={{
          ...props.style,
          opacity: fadeAnim,  
          transform: [{
              translateX: slideUpAnim.interpolate({
                  inputRange: [0, 1],
                  outputRange: [-100, 0]
          })}],
        }}
        
      >
        {props.children}
      </Animated.View>
    );
  }