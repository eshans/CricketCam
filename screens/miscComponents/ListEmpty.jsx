import React from "react";
import { View, Text, Dimensions } from "react-native";
import { Icon } from "native-base";
import { FadeInView } from "./Fade";

export const EmptyComponent = ({icon, mainText, secondaryText, tertiaryText}) => {
  return (
    <FadeInView style={{height: Dimensions.get("window").height}} delay={300} duration={1000}>
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          height: Dimensions.get("window").height,
          padding: 10,
          // backgroundColor: 'red'
        }}
      >
        <Icon name={icon} style={{ fontSize: 70 }} />
        <Text style={{ fontSize: 20, padding: 5 }}>{mainText}</Text>
        <Text style={{ fontSize: 17 }}>{secondaryText}</Text>
        <Text style={{ fontSize: 15, padding: 5 }}>
          {tertiaryText}
        </Text>
      </View>
    </FadeInView>
  );
};
