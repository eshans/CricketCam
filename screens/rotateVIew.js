import React, { Component, useRef } from "react";
import {
  PanResponder,
  View,
  Image,
  Dimensions,
  Text,
  ScrollView,
  TextInput,
  SafeAreaView,
  Select,
  Alert,
} from "react-native";
import { Button, Icon, Picker, Toast } from "native-base";
import { Svg, Line, Circle, Polygon, Text as SvgText } from "react-native-svg";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { Tooltip } from "react-native-elements";
import {
  DEVELOPER_MODE,
  DISABLED_COLOR,
  INITIAL_GRID_PARAMS as initialState,
  INITIAL_POINT_STATE,
  IPHONE_MODE,
  LENGTH_UNIT,
  PRIMARY_COLOR,
  REAL_PITCH_WIDTH,
  SELECT_MARKER_COLORS,
  STUMPS_GAP,
  STYLES,
  TOAST_PROPS,
} from "../constants/values";
import {
  capitalizeFirstLetter,
  drawPitchBorder,
  generateDistanceAreas,
  generateDistanceMarkersLines,
  generateLines,
  generatePitchHorizontalLines,
  generatePitchVerticalLines,
  generateWicketArea,
  generateWicketPoints,
  getValWithScale,
  log,
  ObjectDifference,
  to2DecimalPlaces,
} from "../constants/utils";
import { Value } from "react-native-reanimated";

import { GridControls } from "../components/GridControls";
import { addPoint } from "../components/database/points";
import { getParticipants } from "../components/database/participants";
import DataGrid from "../components/dataGrid";
// import { Picker } from "native-base";
import _ from "lodash";
import { AddPoints } from "../components/AddPoints";
import { editItemJson } from "../components/database/items";
import { FadeInView, SlideDownView } from "./miscComponents/Fade";

const styles = {
  scrollView: {
    maxHeight: 400,
  },
  container: {
    position: "absolute",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    zIndex: 100,
    top: initialState.translateZ,
    // backgroundColor: "blue",
  },

  rotateView: {
    justifyContent: "center",
    alignItems: "center",
    zIndex: 1000,
    // position: "absolute",
    // backgroundColor: "rgba(0, 52, 0, 0.5)",
    zIndex: 100,
    transform: [
      { perspective: initialState.perspective * 100 },
      { rotateX: `${initialState.rotateX}deg` },
    ],
    width: initialState.width * (parseFloat(initialState.scale) / 10),
    height: initialState.height * (parseFloat(initialState.scale) / 10),
  },
  // rotateView2: {
  //   justifyContent: "center",
  //   alignItems: "center",
  //   zIndex: 1000,
  //   position: "absolute",
  //   backgroundColor: "rgba(0, 52, 0, 0.5)",
  //   zIndex: 100,
  //   transform: [
  //     { perspective: 1000 },
  //     { rotateX: `${initialState.rotateX + 20}deg` },
  //   ],
  //   width: initialState.width * (parseFloat(initialState.scale) / 10),
  //   height: initialState.height * (parseFloat(initialState.scale) / 10),
  // },
  topActions: {
    marginRight: 100,
    marginLeft: 100,
    flexDirection: "row",
    // justifyContent: "space-between",
    backgroundColor: "transparent",
    zIndex: 1000,
  },
  controlContainer: {
    padding: 10,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonContainer: {
    flex: 0,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  numberInput: {
    margin: 5,
    borderColor: "#aaaaaa",
    borderWidth: 1,
    borderRadius: 5,
    padding: 7,
    width: 100,
  },

  splitRight: {
    right: 0,
    width: Dimensions.get("window").width / 2,
    position: "fixed",
    zIndex: 1,
    top: 0,
    // overflowX: "hidden",
    paddingTop: 20,
  },
  splitLeft: {
    left: 0,
    width: Dimensions.get("window").width / 2,
    position: "fixed",
    zIndex: 1,
    top: 0,
    // overflowX: "hidden",
    paddingTop: 20,
  },
  centered: {
    position: "absolute",
    alignItems: "center",
    // transform: translate("-50%", "-50%"),
    textAlign: "center",
  },
};

export default class RotateView extends Component {
  state = this.props.savedState
    ? {
        ...this.props.savedState,
        pointColor: initialState.pointColor,
        ...INITIAL_POINT_STATE,
      }
    : { ...initialState, ...INITIAL_POINT_STATE };

  //   {
  //     rotateX: 70,
  //     perspective: 10,
  //     scale: 1,
  //     width: Dimensions.get("window").width / 10,
  //     height: Dimensions.get("window").height / 10,
  //   };

  onStateReset = () => {
    const default_state = this.props.savedState
      ? {
          ...this.props.savedState,
          pointColor: initialState.pointColor,
          ...INITIAL_POINT_STATE,
        }
      : { ...initialState, ...INITIAL_POINT_STATE };

    if (
      true // TODO add object similarity check
    ) {
      Alert.alert(
        `Reset all changes to pitch`,
        "This action is not reversible",
        [
          {
            text: "Cancel",
            onPress: () => log("Cancel Pressed"),
            style: "cancel",
          },
          { text: "Reset", onPress: () => this.setState(default_state) },
        ],
        { cancelable: true }
      );
    } else {
      Toast.show({
        text: "Pitch in default state already!",
        ...TOAST_PROPS("info"),
      });
    }
  };

  updateVideoRecorderParams = (params) => {
    log("updateVideoRecorderParams ");
    this.props.setGridParams(params);
    log('INFO', 'updateVideoRecorderParams', 'file: rotateView.js')
  };

  updateComponentStyles = () => {
    // log("updateComponentStyles ");
    this.refView.setNativeProps({
      style: {
        transform: [
          { perspective: this.state.perspective * 100 },
          { rotateX: `${this.state.rotateX}deg` },
          { rotateY: `${this.state.rotateY}deg` },
          { rotateZ: `${this.state.rotateZ}deg` },
          { translateX: this.state.translateX },
          { translateY: this.state.translateY },
        ],
        width: (this.state.width * parseFloat(this.state.scale)) / 10,
        height: (this.state.height * parseFloat(this.state.scale)) / 10,
      },
    });
    this.refContainer.setNativeProps({
      style: {
        top: this.state.translateZ,
        scale: 0.1,
      },
    });
    log('INFO', 'updateComponentStyles', 'file: rotateView.js')
  };

  onSaveStateToExistingVideo = () => {
    //TODO save only required state keys and values
    editItemJson(this.props.play, this.state);
    log('INFO', 'onSaveStateToExistingVideo', 'file: rotateView.js')
  };

  componentDidMount = () => {
    this.updateComponentStyles();
    getParticipants((arr) => {
      this.setState({ ...this.state, participants: arr });
    });
   log('INFO', 'componentDidMount', 'file: rotateView.js')
    // log({ "rotateview state": this.state });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state !== prevState) {
      this.updateComponentStyles();
      log('INFO', 'componentDidUpdate', 'file: rotateView.js')
      
    }

    if (prevProps.isEditing !== this.props.isEditing) {
      // log("update grid params ", this.state);
      this.updateVideoRecorderParams(this.state);
      
    }
  }

  checkEqualityOfState(state, nextState, field) {
    // log({ state, nextState, field });
    return state[field] === nextState[field];
  }

  shouldComponentUpdate(nextProps, nextState) {
    const consideringState = this.state;
    const consideringNextState = nextState;
    const delFields = ["Comment", "Ball no"];
    // delFields.map((field) => {
    //   delete consideringState[field];
    //   delete consideringNextState[field];
    // });

    // log({
    //   consideringState,
    //   consideringNextState,
    //   zz: _.isEqual(consideringState, consideringNextState),
    //   xprops: this.props == nextProps,
    // });
    // log(new Date());

    // log(
    //   "difference ",
    //   ObjectDifference(consideringState, consideringNextState, delFields)
    // );

    // if (
    //   !ObjectDifference(consideringState, consideringNextState, delFields) &&
    //   this.props == nextProps
    // ) {
    //   log("component not update");
    //   return false;
    // } else {
    //   log("component update");
    return true;
    // }
  }

  // componentDidMount() {
  //   this.refView.setNativeProps({
  //     style: {
  //       transform: [
  //         { perspective: 500 },
  //         { rotateX: "70deg" },
  //         { rotateY: "0deg" },
  //       ],
  //     },
  //   });
  // }

  panResponder = PanResponder.create({
    onMoveShouldSetPanResponder: () => true, //this.props.isEditing,
    onPanResponderMove: this.handlePanResponderMove.bind(this),
  });

  handlePanResponderMove(e, gestureState) {
    const { dx, dy } = gestureState;

    const y = `${dx}deg`;
    const x = `${-dy}deg`;
    this.setState({
      ...this.state,
      pointX: Math.min(
        Math.max(this.state.pointX + dx / 10, 0),
        getValWithScale(this.state.width, this.state.scale)
      ),
      pointY: Math.min(
        Math.max(this.state.pointY + dy / 10, 0),
        getValWithScale(this.state.height, this.state.scale)
      ),
    });
  }

  onChangeValue = (val, step, fineAdj) => {
    const state = this.state;
    var changeVal =
      val === "perspective"
        ? this.state.perspective / 10
        : val === "scale"
        ? 0.2
        : val.includes("translate")
        ? 10
        : ["width", "height"].includes(val)
        ? 100
        : 1;
    var changeVal = fineAdj ? changeVal / 10 : changeVal;
    const newVal =
      step === "increment"
        ? this.state[val] + changeVal
        : this.state[val] - changeVal;

    state[val] = newVal;
    this.setState(state);
    // log(this.state, parseFloat(this.state.scale) / 10);
    // this.refView.setNativeProps({
    //   style: {
    //     transform: [
    //       { perspective: this.state.perspective * 100 },
    //       { rotateX: `${this.state.rotateX}deg` },
    //       { rotateY: `${this.state.rotateY}deg` },
    //       { rotateZ: `${this.state.rotateZ}deg` },
    //       { translateX: this.state.translateX },
    //       { translateY: this.state.translateY },
    //     ],
    //     width: (this.state.width * parseFloat(this.state.scale)) / 10,
    //     height: (this.state.height * parseFloat(this.state.scale)) / 10,
    //   },
    // });
    // this.refContainer.setNativeProps({
    //   style: {
    //     top: this.state.translateZ,
    //   },
    // });
  };

  onReset = (val) => {
    const state = this.state;
    state[val] = initialState[val];
    this.setState(state);
    // log(this.state, parseFloat(this.state.scale) / 10);
    this.refView.setNativeProps({
      style: {
        transform: [
          { perspective: this.state.perspective * 100 },
          { rotateX: `${this.state.rotateX}deg` },
          { rotateY: `${this.state.rotateY}deg` },
          { rotateZ: `${this.state.rotateZ}deg` },
          { translateX: this.state.translateX },
          { translateY: this.state.translateY },
        ],
        width: getValWithScale(this.state.width, this.state.scale),
        height: getValWithScale(this.state.height, this.state.scale),
      },
    });
    this.refContainer.setNativeProps({
      style: {
        top: this.state.translateZ,
      },
    });
  };

  render() {
    // log("saved state load ", this.props.savedState);
    // log(this.state);
    const generateGridPoints = (sub) => {
      const X = this.state.XGrids;
      const Y = this.state.YGrids;

      const Xn = sub ? X * this.state.XSubGrids : X;
      const Yn = sub ? Y * this.state.YSubGrids : Y;
      const ratio = Xn / Yn;
      const Xt = this.state.width;
      const Yt = this.state.height;

      const XScale = Xt / Xn;
      const YScale = Yt / Yn;

      const startingPoint = { x: 0, y: 0 };

      const columns = [];

      const columnsInit = Array.from({ length: Yn }, (_, index) => index);
      const rowsInit = Array.from({ length: Xn }, (_, index) => index);

      columnsInit.push(Yn);
      rowsInit.push(Xn);

      const points = columnsInit.map((column) => {
        const rows = [];
        rowsInit.map((row) => {
          rows.push({
            x: startingPoint.x + row * XScale,
            y: startingPoint.y + column * YScale,
          });
        });
        columns.push(rows);
      });
      return columns;
    };

    const mainGridPoints = generateGridPoints(false);
    const subGridPoints = generateGridPoints(true);

    const isSummary = this.props.screen == 'summary'

    return (
      <>
        {this.props.isEditing && (
          <SlideDownView
            key={"main_view"}
            style={{
              position: "absolute",
              width: Dimensions.get("window").width,
              top: 0,
              flex: 1,
              justifyContent: "top",
              alignItems: "center",
              backgroundColor: "white",
              zIndex: 3000,
              borderRadius: 10
            }}
          >
            {DEVELOPER_MODE && (
              <Button onPress={() => this.setState({ canAddPoints: true })}>
                <Text>show addpoint</Text>
              </Button>
            )}

            <GridControls
              key={"grid_controls"}
              onChangeValue={this.onChangeValue}
              onReset={this.onReset}
              state={this.state}
              setState={(state) => this.setState(state)}
              onStateReset={this.onStateReset}
              screen={this.props.screen}
              onSaveStateToExistingVideo={
                this.props.screen == "addPoint"
                  ? this.onSaveStateToExistingVideo
                  : () => {
                      log("no existing video state");
                    }
              }
            />
          </SlideDownView>
        )}

        <View
          key={"addpoints-view5"}
          style={{
            position: "absolute",
            width: Dimensions.get("window").width,
            top: 0,
            flex: 1,
            justifyContent: "top",
            alignItems: "center",
            backgroundColor: "white",
            zIndex: 3000,
          }}
        >
          <AddPoints
            key={"addpoints-main"}
            pointX={this.state.pointX}
            pointY={this.state.pointY}
            participant={this.state.participant}
            play={this.props.play}
            show={this.props.canAddPoints || this.state.canAddPoints}
            scale={this.state.scale}
          />
          {DEVELOPER_MODE && this.state.canAddPoints && (
            <Button onPress={() => this.setState({ canAddPoints: false })}>
              <Text>hide addpoint</Text>
            </Button>
          )}
        </View>

        <View
          onTouchStart={(e) => {
            // log("touchMove", e.nativeEvent);
            // this.setState({pointX: e.nativeEvent.locationX, pointY: e.nativeEvent.locationY})
            this.setState({
              ...this.state,
              pointX: Math.min(
                Math.max(e.nativeEvent.locationX, 0),
                getValWithScale(this.state.width, this.state.scale)
              ),
              pointY: Math.min(
                Math.max(e.nativeEvent.locationY, 0),
                getValWithScale(this.state.height, this.state.scale)
              ),
            });
          }}
          key={"panresponderview"}
          ref={(component) => (this.refContainer = component)}
          style={styles.container}
          {...this.panResponder.panHandlers}
        >
          <View
            key={"panresponderview2"}
            ref={(component) => (this.refView = component)}
            style={{ ...styles.rotateView }}
          >
            <Svg>
              {/* {true && drawPitchBorder(this.state.height,
                  this.state.width,
                  this.state.scale).map((point, index) => (
                  <Line
                    key={index}
                    x1={point[0].x}
                    y1={point[0].y}
                    x2={point[1].x}
                    y2={point[1].y}
                    stroke="white"
                    strokeWidth={`${parseFloat(this.state.scale) / 3}`}
                  />
                ))} */}
              {/* {true &&
                generateDistanceAreas(this.state.scale).map((area) => (
                  <Polygon
                    key={area.points}
                    points={area.points}
                    fill='red'
                    stroke="purple"
                    strokeWidth="0"
                  />
                ))} */}

              {true &&
                generateDistanceAreas(this.state.scale, isSummary).map((area) => (
                  <Polygon
                    key={area.points}
                    points={area.points}
                    fill={area.fill}
                    stroke="purple"
                    strokeWidth="0"
                  />
                ))}
              {isSummary &&
                generateDistanceMarkersLines(this.state.scale).map(
                  (point, index) => (
                    <Line
                      key={index}
                      x1={point[0].x}
                      y1={point[0].y}
                      x2={point[1].x}
                      y2={point[1].y}
                      stroke="white"
                      strokeWidth={`${(parseFloat(this.state.scale) * 10) / 3}`}
                    />
                  )
                )}
                {true &&
                generateDistanceMarkersLines(this.state.scale, isSummary).map(
                  (point, index) => (
                    <Line
                      key={index}
                      x1={point[0].x}
                      y1={point[0].y}
                      x2={point[1].x}
                      y2={point[1].y}
                      stroke="white"
                      strokeWidth={`${(parseFloat(this.state.scale) * 10) / 3}`}
                    />
                  )
                )}

              {true &&
                generatePitchVerticalLines(this.state.scale).map(
                  (point, index) => (
                    <Line
                      key={index}
                      x1={point[0].x}
                      y1={point[0].y}
                      x2={point[1].x}
                      y2={point[1].y}
                      stroke="white"
                      strokeWidth={`${(parseFloat(this.state.scale) * 10) / 3}`}
                    />
                  )
                )}
              {true &&
                generatePitchHorizontalLines(this.state.scale).map(
                  (point, index) => (
                    <Line
                      key={index}
                      x1={point[0].x}
                      y1={point[0].y}
                      x2={point[1].x}
                      y2={point[1].y}
                      stroke="white"
                      strokeWidth={`${(parseFloat(this.state.scale) * 10) / 2}`}
                    />
                  )
                )}

              {true &&
                generateWicketPoints(this.state.scale).map((point, index) => (
                  <Circle
                    key={`${index}-point`}
                    cx={point.x}
                    cy={point.y}
                    r="2"
                    stroke={"red"}
                    strokeWidth="3"
                    fill="transparent"
                  />
                ))}
              {true &&
                generateWicketArea(this.state.scale).map((area) => (
                  <Polygon
                    key={area.points}
                    points={area.points}
                    fill={area.fill}
                    stroke="purple"
                    strokeWidth="0"
                  />
                ))}

              {/* {!isSummary &&
                generateLines(mainGridPoints).map((point, index) => (
                  <Line
                    key={index}
                    x1={(point[0].x * parseFloat(this.state.scale)) / 10}
                    y1={(point[0].y * parseFloat(this.state.scale)) / 10}
                    x2={(point[1].x * parseFloat(this.state.scale)) / 10}
                    y2={(point[1].y * parseFloat(this.state.scale)) / 10}
                    stroke="white"
                    strokeWidth={`${parseFloat(this.state.scale) / 7}`}
                  />
                ))}
              {!isSummary &&
                generateLines(subGridPoints)
                  .filter((f) => !mainGridPoints.includes(f))
                  .map((point, index) => (
                    <Line
                      key={index}
                      x1={(point[0].x * parseFloat(this.state.scale)) / 10}
                      y1={(point[0].y * parseFloat(this.state.scale)) / 10}
                      x2={(point[1].x * parseFloat(this.state.scale)) / 10}
                      y2={(point[1].y * parseFloat(this.state.scale)) / 10}
                      stroke="white"
                      strokeWidth={`${parseFloat(this.state.scale) / 20}`}
                    />
                  ))} */}
              {this.props.canAddPoints | this.state.canAddPoints ? (
                <Circle
                key="addpoint-point"
                  cx={this.state.pointX}
                  cy={this.state.pointY}
                  r={`${10 - Math.round(6*this.state.pointY/this.state.pitchHeight)}`}
                  stroke={this.state.pointColor}
                  strokeWidth="2"
                  fill={this.state.pointColor}
                />
              ) : null}
              {isSummary
                ? this.props.summaryPoints.map((point) => {
                    const pointJson = point.json;
                    return (
                      <>
                        <Circle
                          onPress={() =>
                            this.props.onPressPoint &&
                            this.props.onPressPoint(point.id)
                          }
                          key={point.id}
                          cx={pointJson.pointX}
                          cy={pointJson.pointY}
                          r="3"
                          stroke={pointJson.pointColor}
                          strokeWidth="5"
                          fill="transparent"
                        />
                        {this.props.showBallNos && (
                          <SvgText
                          key={`${point.id}-number`}
                            onPress={() =>
                              this.props.onPressPoint &&
                              this.props.onPressPoint(point.id)
                            }
                            x={pointJson.pointX}
                            y={pointJson.pointY}
                            stroke="black"
                            fill="black"
                            // fontWeight="bold"
                            textAnchor="left"
                            fontSize="35"
                          >
                            {` ${pointJson["Ball no"]}`}
                          </SvgText>
                        )}
                        {/* <Tooltip popover={<Text>test</Text>}>
                        <Text>test</Text>
                        </Tooltip> */}
                      </>
                    );
                  })
                : null}
            </Svg>
            {/* <Svg
              // ref={(component) => (this.refView = component)}
              style={{ ...styles.rotateView2 }}
            >
              {!isSummary &&
                generatePitch(this.state.height, this.state.width).map(
                  (point, index) => (
                    <Line
                      key={index}
                      x1={(point[0].x * parseFloat(this.state.scale)) / 10}
                      y1={(point[0].y * parseFloat(this.state.scale)) / 10}
                      x2={(point[1].x * parseFloat(this.state.scale)) / 10}
                      y2={(point[1].y * parseFloat(this.state.scale)) / 10}
                      stroke="white"
                      strokeWidth={`${parseFloat(this.state.scale) / 7}`}
                    />
                  )
                )}
              {!isSummary &&
                generateWicketPoints(this.state.height, this.state.width).map(
                  (point, index) => (
                    <Circle
                      key={`${index}-point`}
                      cx={(point.x * parseFloat(this.state.scale)) / 10}
                      cy={(point.y * parseFloat(this.state.scale)) / 10}
                      r="2"
                      stroke={"red"}
                      strokeWidth="3"
                      fill="transparent"
                    />
                  )
                )}
            </Svg> */}
          </View>
        </View>
      </>
    );
  }
}
