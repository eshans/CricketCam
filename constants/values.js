import React from "react";
import { Dimensions } from "react-native";
import * as FileSystem from "expo-file-system";

export const DEVELOPER_MODE = true;
export const IPHONE_MODE = false;

const height = Dimensions.get("window").height * 1.0;
const width = Dimensions.get("window").width * 1.0;

export const PITCH_BOX_HEIGHT = 2256.0; //in cm
export const PITCH_BOX_WIDTH = 366.0; //in cm
export const BOUND_BOX_WIDTH = 1366.0; //in cm

export const REAL_PITCH_WIDTH = 305.0;
export const REAL_PITCH_HEIGHT = 2012.0;
export const CREASE_WIDTH = PITCH_BOX_WIDTH;
export const STUMPS_LINE_WIDTH = BOUND_BOX_WIDTH;

export const HEIGHT = height * 10;
export const WIDTH = (HEIGHT * BOUND_BOX_WIDTH) / PITCH_BOX_HEIGHT;
export const PITCH_WIDTH = (HEIGHT * PITCH_BOX_WIDTH) / PITCH_BOX_HEIGHT;
export const SCALE = 1.0;

export const PITCH_COLOR = "rgba(0, 52, 0, 0.5)";

//video list scroll item hieght
export const SCROLL_ITEM_HEIGHT = 65;

export const STUMPS_GAP = 10;
export const BORDER_TO_PITCH_GAP = (122 / REAL_PITCH_HEIGHT) * height;

export const REAL_WIDE_LINE_GAP = 264;

export const REAL_RATIO = height / REAL_PITCH_HEIGHT;

console.log({ height });

export const INITIAL_GRID_PARAMS = {
  rotateX: 0,
  rotateY: 0,
  rotateZ: 0,
  translateX: 0,
  translateY: 0,
  translateZ: 50, //translate Z is done to parent container
  perspective: 100,
  scale2: 1,
  scale: SCALE,
  width: width,
  // ((height + BORDER_TO_PITCH_GAP * 2) * REAL_PITCH_WIDTH) / REAL_PITCH_HEIGHT,
  height: height, //height + BORDER_TO_PITCH_GAP * 2,
  pitchWidth: width,//(height * REAL_PITCH_WIDTH) / REAL_PITCH_HEIGHT,
  pitchHeight: height,
  XGrids: 5,
  YGrids: 15,
  XSubGrids: 5,
  YSubGrids: 5,
  actualLength: REAL_PITCH_HEIGHT, //todo
  actualWidth: REAL_PITCH_WIDTH, //todo
  pointColor: "red",
  participant: null,
  boundBoxWidth: BOUND_BOX_WIDTH,
};

const initial_distance_to_mid_x = 100;
const initial_distance_to_mid_y = 200;
export const m_width = width / 2;
export const m_height = height / 2;

export const BORDER_STATE = {
  actualLength: 20.12,
  actualWidth: 3.66,
  borderPoints: {
    UL: {
      x: m_width - initial_distance_to_mid_x,
      y: m_height - initial_distance_to_mid_y,
    },
    UR: {
      x: m_width + initial_distance_to_mid_x,
      y: m_height - initial_distance_to_mid_y,
    },

    DL: {
      x: m_width - initial_distance_to_mid_x,
      y: m_height + initial_distance_to_mid_y,
    },
    DR: {
      x: m_width + initial_distance_to_mid_x,
      y: m_height + initial_distance_to_mid_y,
    },
    MS: {
      x: m_width, y:0
    }
  },
  ...INITIAL_POINT_STATE,
  scale: 1
};
const borderRadius = 10;
const bottomStyles = {
  borderWidth: 0.5,
  borderColor: "rgb(100, 100, 200)",
  borderRadius: 10,
  backgroundColor: "rgba(230, 230, 240, 0.9)",
};

export const STYLES = {
  bottomActions: {
    position: "absolute",
    bottom: 0,
    zIndex: 2000,
    height: 60,
    alignItems: "center",
    // justifyContent: "space-around",
    // margin: 10,
    padding: 0,
    flex: 1,
    flexDirection: "row",
    width: Dimensions.get("window").width,
    justifyContent: "space-between",
    ...bottomStyles,
  },
  numberInput: {
    margin: 2,
    borderColor: "#aaaaaa",
    borderWidth: 1,
    borderRadius: 5,
    padding: 7,
    width: 100,
  },
  videoPlayer: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    alignSelf: "center",
  },
  input: {
    margin: 5,
    borderColor: "#aaaaaa",
    borderWidth: 1,
    borderRadius: 5,
    padding: 7,
    width: 100,
  },
  baseButton: {
    borderRadius: borderRadius,
    width: 130,
  },
  baseHeader: bottomStyles,
};

export const LENGTH_UNIT = " cm";

export const SELECT_MARKER_COLORS = [
  ["red", "orange", "yellow"],
  ["blue", "green", "purple"],
];

export const TOAST_PROPS = (type) => {
  return {
    textStyle: {
      textAlign: "center",
    },
    position: "top",
    style: {
      width: 200,
      alignSelf: "center",
      backgroundColor:
        type === "error"
          ? "rgba(50, 0, 0, 0.8)"
          : type === "success"
          ? "rgba(0, 50, 0, 0.8)"
          : type === "info"
          ? "rgba(0, 0, 50, 0.8)"
          : "rgba(0, 0, 0, 0.8)",
      alignItems: "center",
      alignContent: "center",
    },
  };
};

export const VIDEO_LOCATION = `${FileSystem.documentDirectory}videos`;

export const INITIAL_POINT_STATE = {
  pointX: 1000,
  pointY: 1000,
  "Ball no": "1",
  Type: "",
  Runup: "Over",
  Batter: "Right",
  Comment: "",
};

export const PRIMARY_COLOR = "rgb(100, 100, 200)";
export const SECONDARY_COLOR = "lightred";
export const DISABLED_COLOR = "darkgrey";

export const SUMMARY_ROTATEVIEW_SCALE = 1.3;

export const BATTER_POSITIONS = ["Right", "Left"];

export const ADMIN_PASSWORD = "Eshan_Subasinghe";

export const VIDEO_SEEK_INTERVAL = 50; //interval between repeated seek button presses in milliseconds

export const VIDEO_CHANGE_RATE_INTERVAL = 25; //interval between changing video rate
