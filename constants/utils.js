import { Toast } from "native-base";
import { Alert } from "react-native";
import { deleteParticipant } from "../components/database/participants";
import {
  BORDER_TO_PITCH_GAP,
  REAL_PITCH_HEIGHT,
  REAL_PITCH_WIDTH,
  STUMPS_GAP,
  REAL_WIDE_LINE_GAP,
  HEIGHT,
  PITCH_BOX_HEIGHT,
  PITCH_BOX_WIDTH,
  WIDTH,
  SCALE,
  PITCH_WIDTH,
  CREASE_WIDTH,
  STUMPS_LINE_WIDTH,
  PITCH_COLOR,
  TOAST_PROPS,
  DEVELOPER_MODE,
} from "./values";

export const to2DecimalPlaces = (number) => {
  return Math.round(number * 100) / 100;
};

export const to1DecimalPlaces = (number) => {
  return Math.round(number * 10) / 10;
};

export function capitalizeFirstLetter(string) {
  return string && string.charAt(0).toUpperCase() + string.slice(1);
}

export const generateLines = (gridPoints) => {
  const Y = gridPoints.length;
  const X = gridPoints[0].length;

  const lines = [];

  gridPoints[0].map((p, index) => {
    lines.push([p, gridPoints[Y - 1][index]]);
  });
  gridPoints.map((g, index) => {
    lines.push([g[0], g[X - 1]]);
  });

  //log({ lines });
  return lines;
};

export const generatePitchVerticalLines = (scale) => {
  const H = getValWithScale(HEIGHT, scale);
  const W = getValWithScale(WIDTH, scale);
  const midPoint = getValWithScale(WIDTH / 2, scale);
  const hieghtMidPoint = getValWithScale(HEIGHT / 2, scale);
  const REAL_RATIO = H / PITCH_BOX_HEIGHT;

  //borders
  // lines.push(...generateLines([
  //   [
  //     { x: BORDER_TO_PITCH_GAP, y: BORDER_TO_PITCH_GAP },
  //     { x: BORDER_TO_PITCH_GAP, y: height - BORDER_TO_PITCH_GAP },
  //   ],
  //   [
  //     { x: width - BORDER_TO_PITCH_GAP, y: BORDER_TO_PITCH_GAP },
  //     { x: width - BORDER_TO_PITCH_GAP, y: height - BORDER_TO_PITCH_GAP },
  //   ],
  // ]))

  //creases
  const longCreaseWidth = 366;
  const creaseWidth = 264;
  const distanceFromWicket = 122;

  const lines = [
    [
      { x: midPoint - (creaseWidth / 2) * REAL_RATIO, y: H },
      {
        x: midPoint - (creaseWidth / 2) * REAL_RATIO,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT + REAL_PITCH_HEIGHT)) /
            2 -
          (H / PITCH_BOX_HEIGHT) * distanceFromWicket,
      },
    ],
    [
      { x: midPoint + (creaseWidth / 2) * REAL_RATIO, y: H },
      {
        x: midPoint + (creaseWidth / 2) * REAL_RATIO,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT + REAL_PITCH_HEIGHT)) /
            2 -
          (H / PITCH_BOX_HEIGHT) * distanceFromWicket,
      },
    ],
    [
      { x: midPoint - (creaseWidth / 2) * REAL_RATIO, y: 0 },
      {
        x: midPoint - (creaseWidth / 2) * REAL_RATIO,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) /
            2 +
          (H / PITCH_BOX_HEIGHT) * distanceFromWicket,
      },
    ],
    [
      { x: midPoint + (creaseWidth / 2) * REAL_RATIO, y: 0 },
      {
        x: midPoint + (creaseWidth / 2) * REAL_RATIO,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) /
            2 +
          (H / PITCH_BOX_HEIGHT) * distanceFromWicket,
      },
    ],
  ];

  //log([lines]);

  return lines;
};

export const generatePitchHorizontalLines = (scale) => {
  const H = getValWithScale(HEIGHT, scale);
  const W = getValWithScale(WIDTH, scale);
  const midPoint = getValWithScale(WIDTH / 2, scale);
  const hieghtMidPoint = getValWithScale(HEIGHT / 2, scale);

  const REAL_RATIO = H / PITCH_BOX_HEIGHT;

  const lines = [];
  //borders
  // lines.push(...generateLines([
  //   [
  //     { x: BORDER_TO_PITCH_GAP, y: BORDER_TO_PITCH_GAP },
  //     { x: BORDER_TO_PITCH_GAP, y: height - BORDER_TO_PITCH_GAP },
  //   ],
  //   [
  //     { x: width - BORDER_TO_PITCH_GAP, y: BORDER_TO_PITCH_GAP },
  //     { x: width - BORDER_TO_PITCH_GAP, y: height - BORDER_TO_PITCH_GAP },
  //   ],
  // ]))

  //creases
  const longCreaseWidth = CREASE_WIDTH;
  const creaseWidth = STUMPS_LINE_WIDTH;
  const distanceFromWicket = 122;

  const generateCreaseLines = (yVals, creaseWidth) => {
    const lines = [];
    yVals.map((y) => {
      lines.push([
        {
          x: midPoint - (creaseWidth / 2) * REAL_RATIO,
          y: y,
        },
        {
          x: midPoint + (creaseWidth / 2) * REAL_RATIO,
          y: y,
        },
      ]);
    });

    return lines;
  };

  //popping crease
  lines.push(
    ...generateCreaseLines(
      [
        ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) / 2 +
          (H / PITCH_BOX_HEIGHT) * distanceFromWicket,
        ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT + REAL_PITCH_HEIGHT)) / 2 -
          (H / PITCH_BOX_HEIGHT) * distanceFromWicket,
      ],
      longCreaseWidth
    )
  );

  //bowling crease
  lines.push(
    ...generateCreaseLines(
      [
        ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) / 2,
        ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT + REAL_PITCH_HEIGHT)) / 2,
      ],
      creaseWidth
    )
  );

  // //wide lines
  // lines.push(
  //   [
  //     {
  //       x: midPoint + (REAL_WIDE_LINE_GAP * REAL_RATIO) / 2,
  //       y:
  //         hieghtMidPoint +
  //         (REAL_PITCH_HEIGHT * REAL_RATIO) / 2 -
  //         distanceFromWicket * REAL_RATIO,
  //     },
  //     { x: midPoint + (REAL_WIDE_LINE_GAP * REAL_RATIO) / 2, y: height },
  //   ],
  //   [
  //     {
  //       x: midPoint - (REAL_WIDE_LINE_GAP * REAL_RATIO) / 2,
  //       y:
  //         hieghtMidPoint +
  //         (REAL_PITCH_HEIGHT * REAL_RATIO) / 2 -
  //         distanceFromWicket * REAL_RATIO,
  //     },
  //     { x: midPoint - (REAL_WIDE_LINE_GAP * REAL_RATIO) / 2, y: height },
  //   ],
  //   [
  //     {
  //       x: midPoint + (REAL_WIDE_LINE_GAP * REAL_RATIO) / 2,
  //       y:
  //         hieghtMidPoint -
  //         (REAL_PITCH_HEIGHT * REAL_RATIO) / 2 +
  //         distanceFromWicket * REAL_RATIO,
  //     },
  //     { x: midPoint + (REAL_WIDE_LINE_GAP * REAL_RATIO) / 2, y: 0 },
  //   ],
  //   [
  //     {
  //       x: midPoint - (REAL_WIDE_LINE_GAP * REAL_RATIO) / 2,
  //       y:
  //         hieghtMidPoint -
  //         (REAL_PITCH_HEIGHT * REAL_RATIO) / 2 +
  //         distanceFromWicket * REAL_RATIO,
  //     },
  //     { x: midPoint - (REAL_WIDE_LINE_GAP * REAL_RATIO) / 2, y: 0 },
  //   ]
  // );

  // //log([lines]);

  return lines;
};

export const generateWicketPoints = (scale) => {
  const midpoint = getValWithScale(WIDTH / 2, scale);
  const hieghtMidPoint = getValWithScale(HEIGHT / 2, scale);
  const xValues = [midpoint - STUMPS_GAP, midpoint, midpoint + STUMPS_GAP];
  const yValues = [
    0 +
      ((getValWithScale(HEIGHT, scale) / PITCH_BOX_HEIGHT) *
        (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) /
        2,
    getValWithScale(HEIGHT, scale) -
      ((getValWithScale(HEIGHT, scale) / PITCH_BOX_HEIGHT) *
        (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) /
        2,
  ];

  const points = [];
  yValues.map((y) => {
    const vals = xValues.map((x) => ({ x: x, y: y }));
    points.push(...vals);
  });

  return points;
};

export const ObjectDifference = (object1, object2, removeKeys) => {
  var result = {};
  var keys = Object.keys(object1);

  for (var key in object2) {
    if (!keys.includes(key) && !removeKeys.includes(key)) {
      result[key] = object2[key];
    }
  }

  return !!Object.keys(result)[0];
};

export const getValWithScale = (val, scale) => {
  return (val * 1) / 1;
};

//gives the actual distance to show
export const getYDistanceFromPointY = (pointY, scale) => {
  //log({ HEIGHT, PITCH_BOX_HEIGHT, pointY, scale });
  return to2DecimalPlaces(
    (PITCH_BOX_HEIGHT / getValWithScale(HEIGHT, scale)) * pointY -
      (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT) / 2
  );
};

//gives the actual distance to show
export const getXDistanceFromPointX = (pointX, scale) => {
  //log({ HEIGHT, PITCH_BOX_WIDTH, pointX, scale });
  const mid = getValWithScale(WIDTH / 2, scale);
  return to2DecimalPlaces(
    (PITCH_BOX_WIDTH / getValWithScale(WIDTH, scale)) * pointX -
      (PITCH_BOX_WIDTH / getValWithScale(WIDTH, scale)) * mid
  );
};

//gives the actual distance to store
export const getYFromPointY = (pointY, scale) => {
  //log({ HEIGHT, PITCH_BOX_HEIGHT, pointY, scale });
  return to2DecimalPlaces(
    (PITCH_BOX_HEIGHT / getValWithScale(HEIGHT, scale)) * pointY
  );
};

//gives the actual distance to store
export const getXFromPointX = (pointX, scale) => {
  return to2DecimalPlaces(
    (PITCH_BOX_WIDTH / getValWithScale(WIDTH, scale)) * pointX
  );
};

//gives the pixel distance from stored actual distance
export const getPointYFromY = (pointY, scale) => {
  //log({ HEIGHT, PITCH_BOX_HEIGHT, pointY, scale });
  return to2DecimalPlaces(
    pointY / (PITCH_BOX_HEIGHT / getValWithScale(HEIGHT, scale))
  );
};

//gives the pixel distance from stored actual distance
export const getPointXFromX = (pointX, scale) => {
  return to2DecimalPlaces(
    pointX / (PITCH_BOX_WIDTH / getValWithScale(WIDTH, scale))
  );
};

export const drawPitchBorder = (height, width, scale) => {
  const H = getValWithScale(HEIGHT, scale);
  const W = getValWithScale(WIDTH, scale);

  const lines = [
    [
      {
        x: ((W / PITCH_BOX_WIDTH) * (PITCH_BOX_WIDTH - REAL_PITCH_WIDTH)) / 2,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) / 2,
      },
      {
        x: ((W / PITCH_BOX_WIDTH) * (PITCH_BOX_WIDTH - REAL_PITCH_WIDTH)) / 2,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT + REAL_PITCH_HEIGHT)) / 2,
      },
    ],
    [
      {
        x: ((W / PITCH_BOX_WIDTH) * (PITCH_BOX_WIDTH + REAL_PITCH_WIDTH)) / 2,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) / 2,
      },
      {
        x: ((W / PITCH_BOX_WIDTH) * (PITCH_BOX_WIDTH + REAL_PITCH_WIDTH)) / 2,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT + REAL_PITCH_HEIGHT)) / 2,
      },
    ],
    [
      {
        x: ((W / PITCH_BOX_WIDTH) * (PITCH_BOX_WIDTH - REAL_PITCH_WIDTH)) / 2,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT + REAL_PITCH_HEIGHT)) / 2,
      },
      {
        x: ((W / PITCH_BOX_WIDTH) * (PITCH_BOX_WIDTH + REAL_PITCH_WIDTH)) / 2,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT + REAL_PITCH_HEIGHT)) / 2,
      },
    ],
    [
      {
        x: ((W / PITCH_BOX_WIDTH) * (PITCH_BOX_WIDTH - REAL_PITCH_WIDTH)) / 2,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) / 2,
      },
      {
        x: ((W / PITCH_BOX_WIDTH) * (PITCH_BOX_WIDTH - REAL_PITCH_WIDTH)) / 2,
        y:
          ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT + REAL_PITCH_HEIGHT)) / 2,
      },
    ],
  ];

  // points = [{x: y:}]

  return lines;
};

export const generateDistanceMarkersLines = (scale, isSummary) => {
  const W = getValWithScale(WIDTH, scale);
  const H = getValWithScale(HEIGHT, scale);
  const P_W = getValWithScale(PITCH_WIDTH, scale);
  const midpoint = getValWithScale(WIDTH / 2, scale);
  const y_midpoint = W / 2;

  const distancesFromWicket = isSummary ? [200, 400, 600, 800] : [800];
  const yValues = distancesFromWicket.map(
    (y) =>
      ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) / 2 +
      (H / PITCH_BOX_HEIGHT) * y
  );
  const xValues = [y_midpoint - P_W / 2, y_midpoint + P_W / 2];

  const lines = yValues.map((yVal) => [
    {
      x: xValues[0],
      y: yVal,
    },
    {
      x: xValues[1],
      y: yVal,
    },
  ]);
  return lines;
};

export const getYaluesOfDistanceRanges = (scale, distancesFromWicket) => {
  const W = getValWithScale(WIDTH, scale);
  const H = getValWithScale(HEIGHT, scale);

  const yValues = distancesFromWicket.map(
    (y) =>
      ((H / PITCH_BOX_HEIGHT) * (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) / 2 +
      (H / PITCH_BOX_HEIGHT) * y
  );
  return yValues;
};

export const generateDistanceAreas = (scale, isSummary) => {
  const W = getValWithScale(WIDTH, scale);
  const H = getValWithScale(HEIGHT, scale);
  const P_W = getValWithScale(PITCH_WIDTH, scale);
  const midpoint = getValWithScale(WIDTH / 2, scale);
  const y_midpoint = W / 2;

  const alpha = 0.5;

  const distancesFromWicket = isSummary
    ? ["-122", "0", "200", "400", "600", "800", "1200", "2134"]
    : ["-122", "2134"];

  const yValues = getYaluesOfDistanceRanges(scale, distancesFromWicket);
  const colours = isSummary
    ? [
        PITCH_COLOR,
        `rgba(255,125,0,${alpha})`,
        `rgba(255,255,0,${alpha})`,
        `rgba(0,255,0,${alpha})`,
        `rgba(0,255,0,${alpha})`,
        `rgba(255,0,0,${alpha})`,
        PITCH_COLOR,
      ]
    : [PITCH_COLOR];
  const ranges = yValues
    .map((d, i) =>
      i + 1 < yValues.length
        ? {
            distances: [yValues[i], yValues[i + 1]],
            color: colours[i],
          }
        : null
    )
    .filter((a) => a);

  const xValues = [y_midpoint - P_W / 2, y_midpoint + P_W / 2];

  const generatePointsString = (points) => {
    // const arr = []
    const arr = points.map((point, i) =>
      i == 0
        ? `${xValues[0]},${point} ${xValues[1]},${point}`
        : `${xValues[1]},${point} ${xValues[0]},${point}`
    );
    return arr.join(" ");
  };

  const areas = ranges.map((range, i) => ({
    points: generatePointsString(range["distances"]),
    fill: colours[i],
  }));
  //log(areas);
  return areas;
};

export const generateWicketArea = (scale) => {
  const midpoint = getValWithScale(WIDTH / 2, scale);
  const hieghtMidPoint = getValWithScale(HEIGHT / 2, scale);
  const xValues = [midpoint - STUMPS_GAP, midpoint + STUMPS_GAP];
  const yValues = [
    0 +
      ((getValWithScale(HEIGHT, scale) / PITCH_BOX_HEIGHT) *
        (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) /
        2,
    getValWithScale(HEIGHT, scale) -
      ((getValWithScale(HEIGHT, scale) / PITCH_BOX_HEIGHT) *
        (PITCH_BOX_HEIGHT - REAL_PITCH_HEIGHT)) /
        2,
  ];

  const points = [];
  yValues.map((y) => {
    const vals = xValues.map((x) => `${x},${y}`);
    points.push(...vals);
  });

  return [
    {
      points: `${points[0]} ${points[1]} ${points[3]} ${points[2]}`,
      fill: `rgba(0,0,0,0.2)`,
    },
  ];
};

export const decimalToFraction = (_decimal) => {
  function gcd(a, b) {
    return b ? gcd(b, a % b) : a;
  }

  if (!_decimal) {
    return null;
  }

  if (_decimal == 1) {
    return 1;
  }

  const top = _decimal.toString().replace(/\d+[.]/, "");
  const bottom = Math.pow(10, top.length);
  if (_decimal > 1) {
    top = +top + Math.floor(_decimal) * bottom;
  }
  var x = gcd(top, bottom);
  return `${top / x + "/" + bottom / x}`;
};

const getCurrentDate = (separator = ".") => {
  let newDate = new Date();
  let date = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();

  return `${year}${separator}${
    month < 10 ? `0${month}` : `${month}`
  }${separator}${date}`;
};

const getCurrentTime = () => {
  const time = new Date();
  const out =
    time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
  return out;
};

export const log = (level = "INFO", info, comments = null) => {
  const date = getCurrentDate();

  const time = getCurrentTime();

  if (DEVELOPER_MODE) {
    const logString = `${date} ${time}: ${level} : ${info}  ${
      comments ? comments : ""
    }`;
    console.log(logString);
  }
  return true;
};

export const tryCatchFunction = (func, onSuccessMessage) => {
  try {
    func();
    Toast.show({
      text: onSuccessMessage,
      ...TOAST_PROPS("info"),
    });
  } catch (error) {
    Toast.show({
      text: `Error: ${error.message}`,
      ...TOAST_PROPS("error"),
    });
  }
};

export const tryCatchFunctionAsync = async (func, onSuccessMessage) => {
  try {
    await func();
    Toast.show({
      text: onSuccessMessage,
      ...TOAST_PROPS("info"),
    });
  } catch (error) {
    Toast.show({
      text: `Error: ${error.message}`,
      ...TOAST_PROPS("error"),
    });
  }
};

const getYWhenXGiven = (x, borderPoints) => {
  const ratio =
    (x - borderPoints.UL.x) / (borderPoints.UR.x - borderPoints.UL.x);
  const yVal =
    ratio * (borderPoints.UR.y - borderPoints.UL.y) + borderPoints.UL.y;
  return yVal;
};

export const generatePitchPoints = (borderPoints) => {
  const midX = borderPoints.MS.x;
  const ratio =
    (midX - borderPoints.UL.x) / (borderPoints.UR.x - borderPoints.UL.x);

  return {
    UL: borderPoints.UL,
    UR: borderPoints.UR,

    DL: {
      x: 100,
      y: 1000,
    },
    DR: {
      x: 200,
      y: 1000,
    },
  };
};

export const generatePointsString = (borderPoints) => {
  return `${borderPoints.UL.x},${borderPoints.UL.y} ${borderPoints.UR.x},${borderPoints.UR.y} ${borderPoints.DR.x},${borderPoints.DR.y} ${borderPoints.DL.x},${borderPoints.DL.y}`;
};


export const getWicketY = (borderPoints ,x) => {
  const ratio =
    (x - borderPoints.UL.x) /
    (borderPoints.UR.x -
      borderPoints.UL.x);
  const yVal =
    ratio *
      (borderPoints.UR.y -
        borderPoints.UL.y) +
    borderPoints.UL.y;
  return yVal;
};


export const getYPointDistance = (y, points, actual) => {


  console.log({y, points,actual})

  const UL = points.UL
  const UR = points.UR
  const DL = points.DL
  const DR = points.DR

  const maxDist = UL.y - DL.y
  const pDist = UL.y - y
  const yAct = (pDist / maxDist) * actual



  return yAct
}

export const getXPointDistance = (x, points, actual, y) => {



  const UL = points.UL
  const UR = points.UR
  const DL = points.DL
  const DR = points.DR

  const maxDistUp = UL.x - UR.x
  const maxDistDown = DL.x - DR.x

  const ymaxDist = UL.y - DL.y
  const ypDist = UL.y - y

  const widthAtX = maxDistUp + (maxDistDown - maxDistUp) * ypDist / ymaxDist

  const midXU = (UL.x + UR.x) / 2
  const midXD = (DL.x + DR.x) / 2

  // at y line
  const x1 = UL.x + (DL.x - UL.x) * ypDist / ymaxDist
  const xm = midXU + (midXD - midXU) * ypDist / ymaxDist
  const maxDist = x1 - xm
  const pDist = x - xm
  const xAct = (pDist / maxDist) * actual



  return xAct
}